(function($)
{
    var Accordion = function(element)
    {
        var that = this;
        this.dom_element = $(element);
        var li_elements = this.dom_element.find('li');
        this.elements_length = li_elements.length;
        this.animation_elements = [];
        for (var i = 0; i < this.elements_length; i++)
        {
            this.animation_elements[i] = {
                headline: $(li_elements[i]).find('h3'),
                download_link: $(li_elements[i]).find('.accordion_download'),
                all_link: $(li_elements[i]).find('.accordion_all_link'),
                element: $(li_elements[i]),
                current_width: (i === 0) ? 467 : 53,
                target_width: (i === 0) ? 467 : 53,
                current_opacity: (i === 0) ? 1 : 0,
                target_opacity: (i === 0) ? 1 : 0
            };
            this.animation_elements[i].element.bind('mouseover', (function()
            {
                var index = i;
                return function(event)
                {
                    event.stopPropagation();
                    that.animation_elements[that.current].target_width = 53;
                    that.animation_elements[that.current].target_opacity = 0;
                    that.current = index;
                    that.animation_elements[that.current].target_width = 467;
                    that.animation_elements[that.current].target_opacity = 1;
                };
            })());
        }
        this.current = 0;
        setInterval(function()
        {
            that.animate();
        }, 1000 / 16);
    };

    Accordion.prototype.animate = function()
    {
        for (var i = 0; i < this.elements_length; i++)
        {
            var el = this.animation_elements[i];
            if (el.current_width !== el.target_width)
            {
                var new_width = el.current_width - (el.current_width - el.target_width) * 0.3;
                el.current_width = (el.current_width < el.target_width) ? Math.min(Math.ceil(new_width), 467) : Math.max(Math.floor(new_width), 53);
                el.element.css('width', el.current_width);
            }
            if (el.current_opacity !== el.target_opacity)
            {

                var new_opacity = 100 * (el.current_opacity - (el.current_opacity - el.target_opacity) * 0.05);
                new_opacity = (el.current_opacity < el.target_opacity) ? Math.min(Math.ceil(new_opacity), 100) : Math.max(Math.floor(new_opacity), 0);
                el.current_opacity = new_opacity / 100;
                el.headline.css('opacity', el.current_opacity);
                if (el.download_link.length)
                {
                    el.download_link.css('opacity', el.current_opacity);
                }
                if (el.all_link.length)
                {
                    el.all_link.css('opacity', el.current_opacity);
                }
            }
        }
    };

    jQuery(function()
    {
    // TSC EDIT
        new Accordion('.js_accordion');
    //    new Accordion(jQuery('.js_accordion')[0]);
    });

})(jQuery);