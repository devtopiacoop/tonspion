(function ($) {
  
// GLOBALS
Drupal.appConf = {};
Drupal.queryMedia = {};
Drupal.lastScrollPos = 0;


// FUNCTIONS AND DRUPAL METHODS
// ----------------------------------------------------------------------------------------------------

Drupal.queryMedia.get_width = function () {
	return $(window).width();
};

Drupal.queryMedia.get_name = function () {
	var w = this.get_width();
	if (w <= 320) {
		return "IPHONE3";
	} else if (w <= 480) {
		return "IPHONE3-LANDSCAPE";
	} else if (w <= 640) {
		return "IPHONE4";
	} else if (w <= 767) {
		return "IPHONE4";
	} else if (w <= 768) {
		return "IPAD";
	} else if (w <= 1024) {
		return "IPAD-LANDSCAPE";
	} else if (w <= 1280) {
		return "NOTEBOOK";
	} else {
		return "COMPUTER";
	}
};

Drupal.queryMedia.get_group = function () {
	var n = this.get_name();
	if (n == "IPHONE3" || n == "IPHONE3-LANDSCAPE" || n == "IPHONE4" || n == "IPHONE4-LANDSCAPE") {
		return "MOBILE";
	} else if (n == "IPAD" || n == "IPAD-LANDSCAPE") {
		return "TABLET";
	} else if (n == "NOTEBOOK") {
		return "NOTEBOOK"
	} else {
		return "COMPUTER"
	}
};

Drupal.initStart = function(){
  // console.log("Ignition Sequence Start...");
  // console.log(Drupal);

  $(window).resize(function() {
    setTimeout("resize()",500);
  });    
};
Drupal.initResponsive = function(){
 if(Drupal.settings.activeResponsive == '1'){
    Drupal.appConf.ADAPT_CONFIG = {
      path: Drupal.settings.basePath + Drupal.settings.pathToTheme + '/css/',
      dynamic: true,
      //callback: myCallback,
      range: [
        //'  0px to 760px  = main_mobile.css',
        // jorge: Custom Tecnilogica Mobile CSS
        '0 to 568px  = main_mobile.css',
        '760px to 980px  = main_tablet.css'
        //'568px            = main.css',
        //'760px            = main.css'
      ]
    };

    // Navigation
    
    // Events
    addOrientationBodyClass();

    $(window).bind('orientationchange', function(){
      addOrientationBodyClass();
      resize();
    });
  
    adaptJS(window, window.document, Drupal.appConf.ADAPT_CONFIG);
 }

 setTimeout("resize()",500);
};

Drupal.dropDown_Filters= function(){
  $('.dropdown_filter').each(function(){
    var $this = $(this); 
    $this.find("label").addClass("selected");
    $this.find(".views-widget").addClass("drop");
    var dropdownlist  = $this;
    var drop      = dropdownlist.find(".drop");
    var selected    = dropdownlist.find(".selected"); 

    drop.hide();

    selected.bind("click", function(e){
      e.stopPropagation();
      
      if(drop.hasClass('open')){
        drop.slideUp();
        drop.removeClass('open');
        selected.removeClass('open');
      } else {
        drop.slideDown();
        drop.addClass('open');
        selected.addClass('open');
      }
    });
    $("body").bind("click", function(e){
      if(drop.hasClass('open')){
        e.stopPropagation();

        drop.slideUp(0);
        drop.removeClass('open');
        selected.removeClass('open');
      }
    });
  });
};

Drupal.sortBy_Filter = function(){
  $('.sortby_filter').each(function(){
    var baseURL = $("link[rel='canonical']").attr('href')+'?genre='+$(".dropdown_filter input[name='genre']").val()+'&sort_by=';
    var $this = $(this); 
    $this.find("label").addClass("sortby_titlefilter");
    var filter  = $this;
    var titlefilter = filter.find(".sortby_titlefilter");
    var select    = filter.find("select"); 

    select.css('display','none');

    titlefilter.after('<ul class="sortby_list"></ul>');

    $(this).find("select option").each(function(i,e){
      var sel = ($(this).attr('selected'))?' selected':'';
      $this.find(".sortby_list").append('<li class="sortby_list_item_'+i+sel+'"><a href="'+baseURL+$(this).val()+'">'+$(this).text()+'</a></li>');
    })
  });
}

Drupal.dropDownNews_Filters = function(){
  $('.dropdown_filter').each(function(){
    var $this = $(this); 
    var dropdownlist  = $this;
    var drop      = dropdownlist.find(".drop");
    var selected    = dropdownlist.find(".selected"); 

    drop.hide();

    selected.bind("click", function(e){
      e.stopPropagation();
      
      if(drop.hasClass('open')){
        drop.slideUp();
        drop.removeClass('open');
        selected.removeClass('open');
      } else {
        drop.slideDown();
        drop.addClass('open');
        selected.addClass('open');
      }
    });
    $("body").bind("click", function(e){
      if(drop.hasClass('open')){
        e.stopPropagation();

        drop.slideUp(0);
        drop.removeClass('open');
        selected.removeClass('open');
      }
    });
  });
};

Drupal.newsHistory_Filter = function(){
  if($("#block-tonspion-tonspion-news-filter").length > 0 || $("#block-tonspion-tonspion-archive-filter").length > 0){
    //Hide all inside items
    $('.item-list-inside').hide();

    //Activate latests year/month if necessary
    if($("li.year.active").length <= 0){
      $("li.year").first().addClass('active');
      $("li.year").first().find('.item-list-inside').show();
    } else {
      $("li.year.active").find('.item-list-inside').show();
    }
  } 
}

Drupal.detail_Behaviour = function(){
  if($('.detail-behaviour').length > 0){
    $('.detail-behaviour').each(function(){
      $(this).addClass('detail-behaviour-closed');
      $(this).css('position','relative');

      var showdetails = false;

      if($(this).parent().parent().parent().hasClass('event-artist')){
        if($(this).find('.field-name-code-artist-concerts-list').length > 0){
          $(this).find('.field-name-code-artist-concerts-list').hide();
          showdetails = true;         
        }
      } else {
        if($(this).find('.group-right').length > 0){
          $(this).find('.group-right').hide();
          showdetails = true;         
        }
      }

      if(showdetails){
        $(this).append('<a href="#" class="detail-behaviour-action">'+Drupal.t("Details")+'</a>');      
      } 
    });

    $(".detail-behaviour a.detail-behaviour-action").each(function(){
      $(this).bind("click", function(e){
        e.preventDefault();

        if($(this).parent().parent().parent().parent().hasClass('event-artist')){
          var el = $(this).parent().find('.field-name-code-artist-concerts-list');
        } else {
          var el = $(this).parent().find('.group-right');
        }

        if($(this).parent().hasClass('detail-behaviour-closed')){
          el.slideDown("fast");
          $(this).text(Drupal.t("Hide Details"));
          $(this).parent().removeClass('detail-behaviour-closed').addClass('detail-behaviour-open');
        } else {
          el.slideUp("fast");
          $(this).text(Drupal.t("Details"));
          $(this).parent().removeClass('detail-behaviour-open').addClass('detail-behaviour-closed');
        }
      });
    });
  }
}

Drupal.banners_Behaviour = function(){
  //Right Banner
  if($('#bannerright .block').length > 0){
    var el = ($('#overcontent').length > 0)?$("#overcontent"):$("#content-wrapper #content");
    var top_pos = el.offset().top;
    var left_pos = el.offset().left;
    var width_grid = $(".container-12").width();
    var width_banner = $("#bannerright").width();
    var space = 20;
    var new_left_pos = left_pos+width_grid+space;
    var window_width = $(window).width();

    $("#bannerright").css('top',top_pos);
    $("#bannerright").css('left',new_left_pos);
    $("#bannerright").css('right','auto');
    if((window_width-new_left_pos) >= (width_banner/5)){
      $("#bannerright").css('display','block');
    } else {
      $("#bannerright").css('display','none');
    }
  }

  //Left Banner
  if($('#bannerleft .block').length > 0){
    var el = ($('#overcontent').length > 0)?$("#overcontent"):$("#content-wrapper #content");
    var top_pos = el.offset().top;
    var left_pos = el.offset().left;
    var width_banner = $("#bannerleft").width();
    var space = 20;
    var new_left_pos = left_pos-(width_banner+space);

    $("#bannerleft").css('top',top_pos);
    $("#bannerleft").css('left',new_left_pos);
    $("#bannerleft").css('right','auto');
    if(left_pos >= (width_banner/5)){
      $("#bannerleft").css('display','block');    
    } else {
      $("#bannerleft").css('display','none');    
    }
  }
}

Drupal.mp3player_Behaviour = function(){
  if($('.field-name-code-mp3-audio-player .player').length > 0){
    // First state. Paused.

    $('.field-name-code-mp3-audio-player .player').addClass('paused');

    $('.field-name-code-mp3-audio-player .player button.mp3-player-play').bind("click", function(e){
        e.stopPropagation();
        
        // Stop previous playing 
        $('.field-name-code-mp3-audio-player .player.playing').each(function(i,n){
          $(this).removeClass('playing').addClass('paused');
          var audio = ($(this).find('audio').attr('id'));
          document.getElementById(audio).pause();
        });

        $(this).parent().parent().removeClass('paused').addClass('playing');
    });
    $('.field-name-code-mp3-audio-player .player button.mp3-player-pause').bind("click", function(e){
        //e.stopPropagation();
        $(this).parent().parent().removeClass('playing').addClass('paused');
    });
  }
}

Drupal.manipulateDrupalHTML = function(){
  //Artists list
  if($('#sidebar-right .view-artists').length > 0){
    $('#sidebar-right .block-quicktabs .quicktabs-tabpage').each(function(){
      $(this).find('ol li .field-name-title').each(function(i,n){
        //alert($(this).text());
        $(this).parent().prepend('<div class="numol">'+(i + 1)+'</div>');
      });
    });
  }

  //Fix news list type for genre
  $('.view-news .group-right .field-name-field-news-type').removeClass('field-name-field-news-type').addClass('field-name-field-genre');

  //Genre Tags
  if($('*[class*="field-genre"]').length > 0){
    $('*[class*="field-genre"]').each(function(i,n){
      var t = $(this).text();
      var h = '<span>'+t+'</span>';
      h = h.replace(', ','</span><span>');

      $(this).html(h);
    });
  }

  //Assigns DropDown Filters
  $("#edit-genre-wrapper").addClass('dropdown_filter');
  $("#edit-genre-wrapper label").addClass("selected");
  $("#edit-genre-wrapper .views-widget").addClass("drop");

  //Assigns Sortby Filters
  $(".views-widget-sort-by").addClass('sortby_filter');

  //Assigns News History Filter
  $("body.page-news #block-tonspion-tonspion-news-filter").addClass('dropdown_filter');
  $("body.page-news #block-tonspion-tonspion-news-filter .item-list .item-list").removeClass('item-list').addClass('item-list-inside');
  $("body.page-news #block-tonspion-tonspion-news-filter ul.year").removeClass('year').addClass('year-inside');
  $("body.page-news #block-tonspion-tonspion-news-filter div.item-list").before('<label class="selected">'+Drupal.t('News by year')+'</label>');
  $("body.page-news #block-tonspion-tonspion-news-filter div.item-list").addClass("drop");
  $("body.page-news #block-tonspion-tonspion-news-filter div.item-list").next().addClass("filter_link");

  //Assigns News Archive History Filter
  $("body.page-news-archive #content-inner").addClass('page-news-archive');
  $("body.page-news-archive .region-content").addClass('archive_filter');
  $("body.page-news-archive .region-content .item-list .item-list").removeClass('item-list').addClass('item-list-inside');
  $("body.page-news-archive .region-content ul.year").removeClass('year').addClass('year-inside');

  //Selected Artists
  var amplia=false;
  var div = '#block-views-e8952cc8588d0180223dcce6dd0b1b6e';
  $(div + ' .field-name-field-ref-artist').each(function(i,e){
    if(i<=1){
      if($(this).text().length > 15){
        amplia = true;
      }
    }
  });
  if(amplia){
    $(div + ' .content').css('margin-top','0px');
  }

  //Active submenu artists
  //Sample: /en/node/120548/news
  if($("#block-menu-menu-artist-submenu").length > 0){
    $("#block-menu-menu-artist-submenu a.active").parent().addClass('active');
  }

  //Assigns 'details' behavior
  $('.view-id-events .view-content .ds-2col.node-teaser').addClass('detail-behaviour');
  $('.view-id-events .view-content .ds-2col-stacked.node-artist').addClass('detail-behaviour');
  // $('.view-id-events .view-content .ds-2col-stacked.node-artist').addClass('ds-2col');
  
  //Media Gallery
  if($('body.node-type-media-street .view-id-media_gallery').length > 0){
    //Vertical image centering
    setTimeout( function() {
      $('body.node-type-media-street .view-id-media_gallery .views_slideshow_slide').each(function(i,e){
          var image = $(e).find('span.field-content img');
          var span = $(e).find('span.field-content');

          var span_h = span.height();
          var i = new Image();
          i.src = image.attr('src');
          var img_h = i.height;

          console.log('Media Gallery - Vertical image centering::: '+ 'src: ' + image.attr('src') + ', image = ' + img_h + ', span = ' + span_h);

          if(img_h < span_h){
            var diff = (span_h-img_h)/2;
            image.css('top',diff);
          } else {
            span.attr('style', 'height: '+img_h+'px !important');
          }
        });

      resfreshHeightGallery();
    }, 1000 );

    //Height Adjust
    $('body.node-type-media-street .view-id-media_gallery .views-slideshow-controls-top .views-slideshow-controls-text-next a').click(function(){
      setTimeout( function() {
        resfreshHeightGallery();
      }, 1000 );
    });
    $('body.node-type-media-street .view-id-media_gallery .views-slideshow-controls-top .views-slideshow-controls-text-previous a').click(function(){
      setTimeout( function() {
        resfreshHeightGallery();
      }, 1000 );
    });

  }

  //:last-child
  $(':last-child').addClass('lastchild');
};

Drupal.initScrollOpacity = function(){
  $(window).scroll(function (e) {
    var minopacity = 0.96;
    var s = $(document).scrollTop();
    var scrollPos = s;
    var min_height = 60;
    var max_height = 110;

    //console.log(s);
    if(s<=40){
      $('header #headerbottom-wrapper').css('opacity','1');
      $('header #headerbottom-wrapper').css('top','0px');
      $('header').css('height',max_height+'px');
      $('header').removeClass('compress');
    } else if(s>40 && s<=470){
      $('header #headerbottom-wrapper').css("opacity",minopacity);
      $('header #headerbottom-wrapper').css('top','0px');
      //$('header').css('height',max_height+'px');
    } else if(s>470){
      $('header').addClass('compress');
      if(scrollPos < Drupal.lastScrollPos){
        //console.log('sube');
        $('header #headerbottom-wrapper').css('top','0px');
        $('header #headerbottom-wrapper').css("opacity",minopacity);
        $('header').css('height',max_height+'px');
      } else {
        //console.log('baja');
        $('header #headerbottom-wrapper').css('top','-80px');
        $('header #headerbottom-wrapper').css("opacity",minopacity);
        $('header').css('height',min_height+'px');
      }
    }

    Drupal.lastScrollPos = scrollPos;
  });
}  


// EXTRA FUNCTIONS
// ----------------------------------------------------------------------------------------------------
resize = function(){
  Drupal.banners_Behaviour();
};

addOrientationBodyClass = function(){
  $('body').removeClass('orientation-landscape');
  $('body').removeClass('orientation-portrait');

  // if(Drupal.queryMedia.get_group() == "MOBILE" || Drupal.queryMedia.get_group() == "TABLET"){
  var n = Drupal.queryMedia.get_name();
  n = n.split("-")[1];

  if(n == "LANDSCAPE") {
    $('body').addClass('orientation-landscape');
  } else {
    $('body').addClass('orientation-portrait');
  }
  // }
};

resfreshHeightGallery = function(){
  var curr = $('.views_slideshow_slide[style*="display: block"]');
  var image = curr.find('span.field-content img');
  var span = curr.find('span.field-content');
  
  console.log("datos: " + span.parent().height() + ", " + span.parent().parent().height());

  var newH = span.parent().height();
  span.parent().parent().height(newH);
  span.parent().parent().parent().height(newH);
};


// DRUPAL BEHAVIORS
// ----------------------------------------------------------------------------------------------------
// Init
Drupal.behaviors.mpgtheme = {
  attach: function(context, settings) {
    Drupal.initStart();
    Drupal.initResponsive();
    Drupal.manipulateDrupalHTML();
    Drupal.initScrollOpacity();
    Drupal.dropDown_Filters();
    Drupal.sortBy_Filter();
    Drupal.newsHistory_Filter();
    Drupal.detail_Behaviour();
    Drupal.banners_Behaviour();
    Drupal.mp3player_Behaviour();
  }
};
  
})(jQuery);
