<?php
/**
 * @file
 * controls load theme.
 */

include_once(drupal_get_path('theme', 'tonspion_theme') . '/inc/override_functions.inc');
include_once(drupal_get_path('theme', 'tonspion_theme') . '/inc/grid_functions.inc');