<?php
/**
 * @file
 * Theme setting callbacks for the tonspion_theme theme.
 */

include_once(drupal_get_path('theme', 'tonspion_theme') . '/inc/override_functions.inc');
include_once(drupal_get_path('theme', 'tonspion_theme') . '/inc/grid_functions.inc');

// Impliments hook_form_system_theme_settings_alter().
function tonspion_theme_form_system_theme_settings_alter(&$form, $form_state) {
	$form['theme_settings']['active_responsive'] = array(
	    '#type' => 'checkbox',
	    '#title' => t('Active Responsive'),
	    '#default_value' => theme_get_setting('active_responsive'),
	);
	$form['theme_settings']['show_grid'] = array(
	    '#type' => 'checkbox',
	    '#title' => t('Show Grid (for development use)'),
	    '#default_value' => theme_get_setting('show_grid'),
	);
	$form['theme_settings']['livereload'] = array(
	    '#type' => 'checkbox',
	    '#title' => t('Active LiveReload Support (for development use). NOTE: Requires LiveReload - http://livereload.com '),
	    '#default_value' => theme_get_setting('livereload'),
	);
}
