<?php

/**
 * @file
 * Overrides views template in order to replace H3 by a DIV, as artist entity is rendered in $title
 * Also groups each row into a DIV. 
 *
 * @ingroup views_templates
 */
?>
<div class="event-artist">
	<?php print $title; ?>
	<div class="event-items">
	<?php foreach ($rows as $id => $row): ?>
	  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
	    <?php print $row; ?>
	  </div>
	<?php endforeach; ?>
	</div>
</div>
