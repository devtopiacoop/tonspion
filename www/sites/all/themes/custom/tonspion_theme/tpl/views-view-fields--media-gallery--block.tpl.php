<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

if ( $fields['type']->content == "Video" ) {
  if ( strpos($fields['uri']->content,'http://') !== false ) {
  	//print $fields['uri']->content;
	print '<iframe width="500" height="330" style="width: 500px; height: 330px;" src="'.$fields['uri']->content.'" frameborder="0" allowfullscreen></iframe>';
  } else {
  	print $fields['rendered']->content;
  }
} elseif ($fields['type']->content == "Widget"||$fields['type']->content == "Steuerelement"||$fields['type']->content == "") {
	$filepath = drupal_realpath($fields['uri']->content);
	print file_get_contents($filepath);
	
} else {	
	print $fields['rendered']->content;
}
print '<div class="gallery-media-text" style="text-align:left;">' . $fields['field_media_text']->content . '</div>';
?>
