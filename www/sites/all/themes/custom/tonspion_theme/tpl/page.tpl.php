<?php

/**
 * @file page.tpl.php
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['slideshow']: Items for the slideshow region.
 * - $page['sidebar_first']: Items for the left sidebar.
 * - $page['sidebar_second']: Items for the right sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see nucleus_preprocess_page()
 */
?>
<div class="page-container">
		<header class="mastheader">
			<div id="headertop-wrapper">
        <div id="headertop" class="container-12">
          <div id="brandhold-wrapper" class="grid-3">
            <div id="brandhold">
                <?php if ($logo): ?>
                  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" id="logocms">
                    <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
                  </a>
                <?php endif; ?>
                <?php if ($site_name || $site_slogan): ?>
                  <div id="name-and-slogan">
                    <?php if ($site_name && $is_front): ?>
                      <h1 class="site-name">
                        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
                          <?php print $site_name; ?>
                        </a>
                      </h1>
                    <?php endif; ?>
                    <?php if ($site_name && !$is_front): ?>
                      <div class="site-name">
                        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
                          <?php print $site_name; ?>
                        </a>
                      </div>
                    <?php endif; ?>
                    <?php if ($site_slogan): ?>
                      <p class="site-slogan"><?php print $site_slogan; ?></p>
                    <?php endif; ?>
                  </div>
                <?php endif; ?>
            </div>
          </div>
  				<div id="headertop-inner" class="grid-9">
  				<?php if ($header_top = render($page['header_top'])): ?>
  					<?php print $header_top; ?>
  				<?php endif; ?>
  				</div>
				</div>
			</div>
			
			<div id="headerbottom-wrapper">
				<div id="headerbottom" class="container-12">
					<?php $header_bottom = render($page['header_bottom']); ?>
					<?php if ($header_bottom || $main_menu || $secondary_menu): ?>
					<div id="headerbottom-inner" class="grid-12">
						<?php if ($header_bottom): ?>
								<?php print $header_bottom; ?>
						<?php else: ?>
								<?php if($main_menu): ?><div id="main-menu-wrapper" class="navigation-default"><?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('menu')))); ?></div><?php endif; ?>
								<?php if($secondary_menu): ?><div id="secondary-menu-wrapper" class="navigation-default"><?php print theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary-menu', 'class' => array('menu')))); ?></div><?php endif; ?>
						<?php endif; ?>
					</div>
					<?php endif; ?>
					<?php if ($active_responsive): ?>
					<!-- <nav class="panel-switch-navigation grid-12">
							<ul>
								<li id="topanelleft" class="left-nav-item"><a href="#left-panel"><?php print t('Settings'); ?></a></li>
								<li id="topanelright" class="right-nav-item"><a href="#right-panel"><?php print t('Menu'); ?></a></li>
								<li id="tohome" class="right-nav-item"><a href="<?php print $front_page; ?>"><?php print t('Home'); ?></a> - </li>
							</ul>
					</nav> -->
					<?php endif; ?>
				</div>
			</div>
		</header>
		<div id="maincontent" class="mastcontent">
			<div id="defaults" class="container-12">
				<?php if ($tabs = render($tabs)): ?>
	        <div class="tabs"><?php print $tabs; ?></div>
	      <?php endif; ?>
				<?php if ($page['highlighted']): ?><div id="highlighted" class="grid-12"><?php print render($page['highlighted']); ?></div><?php endif; ?>
				<?php if ($messages): ?>
				<div id="console" class="grid-11">
				<?php print $messages; ?>
				</div>
				<?php endif; ?>

				<?php if ($page['help']): ?>
				<div id="help" class="grid-12">
				<?php print render($page['help']); ?>
				</div>
				<?php endif; ?>

				<?php if ($action_links): ?>
				<ul class="action-links">
				<?php print render($action_links); ?>
				</ul>
				<?php endif; ?>
			</div>

			<?php $banner_top = render($page['banner_top']); ?>
			<?php if ($banner_top): ?>
			<div id="bannertop" class="container-12">
				<div id="bannertop-inner" class="grid-12">
				<?php if($banner_top){
					print $banner_top;	
				} ?>
				</div>
			</div>
			<?php endif; ?>

			<?php if ($breadcrumb): ?>
			<div id="breadcrumb-wrapper">
				<div id="breadcrumb" class="container-12">
					<div id="breadcrumb-inner" class="grid-12">
					<?php if($over_content){
						print $breadcrumb;
					} ?>
					</div>
				</div>
			</div>
			<?php endif; ?>

			<?php $over_content = render($page['over_content']); ?>
			<?php if ($over_content): ?>
			<div id="overcontent-wrapper">
				<div id="overcontent" class="container-12">
					<div id="overcontent-inner" class="grid-12">
					<?php if($over_content){
						print $over_content;	
					} ?>
					</div>
				</div>
			</div>
			<?php endif; ?>

			<?php if ($content = render($page['content'])): ?>
			<?php $sidebar_left = render($page['sidebar_left']); ?>	
			<?php $sidebar_right = render($page['sidebar_right']); ?>	

			<div id="content-wrapper">
				<div id="content" class="container-12">
					<?php if ($sidebar_left): ?>
					<div id="sidebar-left" class="grid-4">
					<?php print $sidebar_left; ?>
					</div>
					<?php endif; ?>
					<div id="content-inner" class="<?php print ns('grid-12', $sidebar_left, 4, $sidebar_right, 4) ?>">
			      	<?php print render($title_prefix); ?>
			      	<?php if ($title && $is_front): ?>
			      	<h2 class="title" id="page-title">
			          <?php print $title; ?>
			        </h2>
			      	<?php endif; ?>
			      	<?php if ($title && !$is_front): ?>
			        <<?php print $no_title_h1?'span':'h1';?> class="title" id="page-title">
			          <?php print $title; ?>
			        </<?php print $no_title_h1?'span':'h1';?>>
			      	<?php endif; ?>
			      	<?php print render($title_suffix); ?>
						<?php print $content; ?>
					</div>
					<?php if ($sidebar_right): ?>
					<div id="sidebar-right" class="grid-4">
					<?php print $sidebar_right; ?>
					</div>
					<?php endif; ?>
				</div>
			</div>

			<?php $banner_right = render($page['banner_right']); ?>
			<?php if ($banner_right): ?>
			<div id="bannerright">
				<div id="bannerright-inner">
				<?php if($banner_right){
					print $banner_right;	
				} ?>
				</div>
			</div>
			<?php endif; ?>

			<?php $banner_left = render($page['banner_left']); ?>
			<?php if ($banner_left): ?>
			<div id="bannerleft">
				<div id="bannerleft-inner">
				<?php if($banner_left){
					print $banner_left;	
				} ?>
				</div>
			</div>
			<?php endif; ?>

			<?php endif; ?>

			<?php if ($under_content = render($page['under_content'])): ?>
			<div id="undercontent-wrapper">
				<div id="undercontent" class="container-12">
					<div id="undercontent-inner" class="grid-12">
					<?php print $under_content; ?>
					</div>
				</div>
			</div>
			<?php endif; ?>

		</div>

		<footer class="mastfooter">
			<div id="footertop-wrapper">
				<div id="footertop" class="container-12">
					<?php if ($footer_top = render($page['footer_top'])): ?>
					<div id="footertop-inner" class="grid-12">
						<?php	print $footer_top; ?>
					</div>
					<?php endif; ?>
				</div>
			</div>

			<div id="footerbottom-wrapper">
				<div id="footerbottom" class="container-12">
					<?php if ($footer_bottom = render($page['footer_bottom'])): ?>
					<div id="footerbottom-inner" class="grid-12">
						<?php	print $footer_bottom; ?>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</footer>
</div>
