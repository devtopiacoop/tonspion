<?php

/**
 * @file html.tpl.php
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 * @see nucleus_preprocess_html()
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" class="ie lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" class="ie ie7"> <![endif]-->
<!--[if IE 8]>         <html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" class="ie ie8"> <![endif]-->
<!--[if IE 9]>         <html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" class="no-ie"> <!--<![endif]-->
	<head>
		<?php print $head; ?>
		<title><?php print $head_title; ?></title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<!-- <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" /> -->
		<meta name="viewport" content="width=device-width, user-scalable=yes">
		<meta name="HandheldFriendly" content="true" />
		<meta name="apple-touch-fullscreen" content="YES" />
		<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
		<?php print $styles; ?>
		<!--[if IE]>
			<link rel="stylesheet" type="text/css" href="<?php print base_path() . path_to_theme(); ?>/css/ie.css" />
		<![endif]-->
		<?php print $scripts; ?>
		<!--[if lt IE 9]>
		    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		    <script>window.html5 || document.write('<script src="<?php print base_path() . path_to_theme(); ?>/js/libs/vendor/html5shiv.js"><\/script>')</script>
		<![endif]-->
	</head>

  <body class="<?php print $classes; ?>"<?php print $attributes;?>>
    <?php print $page_top; ?>
    <?php print $page; ?>
    <?php print $page_bottom; ?>
	<?php if ($livereload): ?>
		<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
	<?php endif; ?>
  </body>
</html>