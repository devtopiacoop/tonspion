<?php
/**
 * @file
 * Override of the hook theme
 */

/**
* Converts a string to a suitable html ID attribute.
*/
function tonspion_theme_id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  // If the first character is not a-z, add 'n' in front.
  if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
    $string = 'id' . $string;
  }
  return $string;
}


/**
* Humanize slug
*/
function tonspion_theme_humanize($string) {
	$string = trim(strtolower($string));
	$string = preg_replace('/[^a-z0-9\s+]/', ' ', $string);
	$string = preg_replace('/\s+/', ' ', $string);
	$string = explode(' ', $string);

	//	$string = array_map('ucwords', $string);

	return implode(' ', $string);
}


/**
 * Override or insert variables into the html template.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 */
function tonspion_theme_preprocess_html(&$vars) {
  global $theme, $base_url;

	// Classes for body element. Allows advanced theming based on context
  // (home page, node of certain type, etc.)
  $body_classes = array();
  if (!$vars['is_front']) {
    // Add unique classes for each page and website section
    $path = drupal_get_path_alias($_GET['q']);
    list($section, ) = explode('/', $path, 2);
    $body_classes[] = tonspion_theme_id_safe('page-' . $path);
    $body_classes[] = tonspion_theme_id_safe('section-' . $section);
    if (arg(0) == 'node') {
      if (arg(1) == 'add') {
        if ($section == 'node') {
          array_pop($body_classes); // Remove 'section-node'
        }
        $body_classes[] = 'section-node-add'; // Add 'section-node-add'
      }
      elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
        if ($section == 'node') {
          array_pop($body_classes); // Remove 'section-node'
        }
        $body_classes[] = 'section-node-' . arg(2); // Add 'section-node-edit' or 'section-node-delete'
      }
    }
  }
  $vars['classes_array'][] = implode(' ', $body_classes); // Concatenate with spaces

	//Show grid
	$show_grid = theme_get_setting('show_grid');
	if($show_grid){$vars['classes_array'][] = "show-grid";};
	
	//LiveReload
	$vars['livereload'] = theme_get_setting('livereload');

	// Google fonts
  // $element = array(
  //   '#tag' => 'link', // The #tag is the html tag - <link />
  //   '#attributes' => array( // Set up an array of attributes inside the tag
  //     'href' => 'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800|Open+Sans+Condensed:300,700,300italic', 
  //     'rel' => 'stylesheet',
  //     'type' => 'text/css',
  //   ),
  // );
	$element = array(
	  '#tag' => 'link', // The #tag is the html tag - <link />
	  '#attributes' => array( // Set up an array of attributes inside the tag
	    'href' => 'http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700|Open+Sans+Condensed:300,700,300italic|Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800|Montserrat:400,700', 
	    'rel' => 'stylesheet',
	    'type' => 'text/css',
	  ),
	);
	drupal_add_html_head($element, 'google_font_opensans');
}


/**
 * Override or insert variables into the page template.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 */
function tonspion_theme_preprocess_page(&$vars) {

  //dsm($vars);

	//Responsive menu tree
	$pid = variable_get('menu_main_links_source', 'main-menu');
	$tree = menu_tree($pid);
	$tree = str_replace(' class="menu"', '', $tree);
	$vars['main_menu_responsive_panel'] = drupal_render($tree);

	//is home? is logged?
	$vars['is_home']    		= drupal_is_front_page();
	$vars['is_logged']  		= user_is_logged_in();
	
	//Section name
	$path = drupal_get_path_alias($_GET['q']);
  list($section, ) = explode('/', $path, 2);
  $vars['section_name'] = ucfirst(tonspion_theme_humanize($section));

	//Settings to JS
	$vars['active_responsive'] = theme_get_setting('active_responsive');
	drupal_add_js('jQuery.extend(Drupal.settings, { "pathToTheme": "' . path_to_theme() . '" });', 'inline');
	drupal_add_js('jQuery.extend(Drupal.settings, { "activeResponsive": "' . $vars['active_responsive'] . '" });', 'inline');

}


/**
 * Override or insert variables into the maintenance template.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 */
function tonspion_theme_preprocess_maintenance_page(&$vars) {
  global $theme, $base_url;

  //Show grid
  $show_grid = theme_get_setting('show_grid');
  if($show_grid){$vars['classes_array'][] = "show-grid";};
  
  //LiveReload
  $vars['livereload'] = theme_get_setting('livereload');

  // Google fonts
  $element = array(
    '#tag' => 'link', // The #tag is the html tag - <link />
    '#attributes' => array( // Set up an array of attributes inside the tag
      'href' => 'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800|Open+Sans+Condensed:300,700,300italic', 
      'rel' => 'stylesheet',
      'type' => 'text/css',
    ),
  );
  drupal_add_html_head($element, 'google_font_opensans');

  //Settings to JS
  $vars['active_responsive'] = theme_get_setting('active_responsive');
  drupal_add_js('jQuery.extend(Drupal.settings, { "pathToTheme": "' . path_to_theme() . '" });', 'inline');
  drupal_add_js('jQuery.extend(Drupal.settings, { "activeResponsive": "' . $vars['active_responsive'] . '" });', 'inline');
}


/**
 * Override of theme_breadcrumb().
 */
function tonspion_theme_breadcrumb($vars) {
  if (theme_get_setting('breadcrumb_display')) {
    $breadcrumb = $vars['breadcrumb'];
    $home_class = 'crumb-home';
    if (!empty($breadcrumb)) {
      $heading = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
      $separator = " &#187; ";
      $output = '';
      foreach ($breadcrumb as $key => $val) {
        if ($key == 0) {
          $output .= '<li class="crumb ' . $home_class . '">' . $val . '</li>';
        }
        else {
          $output .= '<li class="crumb"><span class="divider">' . $separator . '</span>' . $val . '</li>';
        }
      }
      return $heading . '<ol id="crumbs">' . $output . '</ol>';
    }
  }
  return '';
}

