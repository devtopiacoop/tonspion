	<?php

/**
 * @file media_muzu/themes/media-muzu-video.tpl.php
 *
 * Template file for theme('media_muzu_video').
 *
 * Variables available:
 *  $uri - The media uri for the Muzu video (e.g., muzu://v/xsy7x8c9).
 *  $video_id - The unique identifier of the Muzu video (e.g., xsy7x8c9).
 *  $id - The file entity ID (fid).
 *  $url - The full url including query options for the Muzu iframe.
 *  $options - An array containing the Media Muzu formatter options.
 *  $api_id_attribute - An id attribute if the Javascript API is enabled;
 *  otherwise NULL.
 *  $width - The width value set in Media: Muzu file display options.
 *  $height - The height value set in Media: Muzu file display options.
 *  $title - The Media: Muzu file's title.
 *  $alternative_content - Text to display for browsers that don't support
 *  iframes.
 *
 */

?>
<div class="<?php print $classes; ?> media-muzu-<?php print $id; ?>">
  <iframe class="media-muzu-player" width="<?php print $width; ?>" height="<?php print $height; ?>" title="<?php print $title; ?>" src="<?php print $url; ?>" frameborder="0" allowfullscreen><?php print $alternative_content; ?></iframe>
</div>
