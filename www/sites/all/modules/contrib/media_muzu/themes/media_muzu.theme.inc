<?php

/**
 * @file media_muzu/themes/media_muzu.theme.inc
 *
 * Theme and preprocess functions for Media: Muzu.
 */

/**
 * Preprocess function for theme('media_muzu_video').
 */
function media_muzu_preprocess_media_muzu_video(&$variables) {

  // Build the URI.
  $wrapper = file_stream_wrapper_get_instance_by_uri($variables['uri']);
  $parts = $wrapper->get_parameters();
  $variables['video_id'] = check_plain($parts['v']);
  $url_base = 'muzu.tv';

  // Make the file object available.
  $file_object = file_uri_to_object($variables['uri']);

  //Ex: http://www.muzu.tv/api/video/details/?muzuid=qrEF3lEvZv&id=16356&format=xml

  //Get current extra params from Muzu API
  $options['muzuid'] = variable_get('media_muzu__api_key','');
  $options['format'] = 'json';
  $options['id'] = check_plain($parts['v']);

  $url = url(MEDIA_MUZU_REST_API . '/video/details', array('query' => $options));

  $response = drupal_http_request($url);
  if (!isset($response->error)) {
    $data = drupal_json_decode($response->data);
  }
  else {
    throw new Exception("Error Processing Request. (Error: {$response->code}, {$response->error})");
    return;
  }

  // Parse options and build the query string. Only add the option to the query
  // array if the option value is not default. Be careful, depending on the
  // wording in media_muzu.formatters.inc, TRUE may be query=0.
  $query = array();

  // Make css z-index work with flash object. Must be the first parameter.
  $query['wmode'] = 'opaque';

  // These queries default to 0. If the option is true, set value to 'y'.
  foreach (array('autostart','soundoff') as $option) {
    if ($variables['options'][$option]) {
      $query[$option] = 'y';
    }
  }

  // These queries default to 1. If the option is false, set value to 0.
  // (None right now.)

  // These queries default to 1. Option wording is reversed, so if the option
  // is true, set value to 0.
  // (None right now.)

  $protocol = $variables['options']['protocol'];

  // Add some options as their own template variables.
  foreach (array('width', 'height') as $theme_var) {
    $variables[$theme_var] = $variables['options'][$theme_var];
  }

  // Do something useful with the overridden attributes from the file
  // object. We ignore alt and style for now.
  if (isset($variables['options']['attributes']['class'])) {
    if (is_array($variables['options']['attributes']['class'])) {
      $variables['classes_array'] = array_merge($variables['classes_array'], $variables['options']['attributes']['class']);
    }
    else {
      // Provide nominal support for Media 1.x.
      $variables['classes_array'][] = $variables['options']['attributes']['class'];
    }
  }

  // Add template variables for accessibility.
  $variables['title'] = check_plain($file_object->filename);
  $variables['alternative_content'] = $data[0]['description'];

  // Build the iframe URL with options query string.
  $variables['url'] = url($protocol . '//www.' . $url_base . '/player/getPlayer/i/'. $data[0]['SourceId'] .'/' . $variables['video_id'], array('query' => $query, 'external' => TRUE));
}
