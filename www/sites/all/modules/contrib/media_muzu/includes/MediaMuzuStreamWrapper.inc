<?php

/**
 *  @file media_muzu/includes/MediaMuzuStreamWrapper.inc
 *
 *  Create a Muzu Stream Wrapper class for the Media/Resource module.
 */

/**
 *  Create an instance like this:
 *  $muzu = new MediaMuzuStreamWrapper('muzu://v/[video-id]');
 */
class MediaMuzuStreamWrapper extends MediaReadOnlyStreamWrapper {

  // Overrides $base_url defined in MediaReadOnlyStreamWrapper.
  protected $base_url = 'http://www.muzu.tv/';

  /**
   * Returns a url in the format "http://www.muzu.tv/[artist-name]/[track-name]/[video-id]]".
   *
   * Overrides interpolateUrl() defined in MediaReadOnlyStreamWrapper.
   * This is an exact copy of the function in MediaReadOnlyStreamWrapper,
   * here in case that example is redefined or removed.
   *
   * @return String URL
   */
  function interpolateUrl() {
    if ($parameters = $this->get_parameters()) {

      $options['muzuid'] = variable_get('media_muzu__api_key','');
      $options['format'] = 'json';
      $options['id'] = check_plain($parameters['v']);

      $url = url(MEDIA_MUZU_REST_API . '/video/details', array('query' => $options));

      $response = drupal_http_request($url);
      if (!isset($response->error)) {
        $data = drupal_json_decode($response->data);
        return $data[0]['link'];
      }
      else {
        throw new Exception("Error Processing Request. (Error: {$response->code}, {$response->error})");
        return;
      }

      //return $this->base_url . '?' . http_build_query($parameters);
    }
  }

  static function getMimeType($uri, $mapping = NULL) {
    return 'video/muzu';
  }

  function getTarget($f) {
    return FALSE;
  }

  function getOriginalThumbnailPath() {
    $parts = $this->get_parameters();
    $id = check_plain($parts['v']);

    $options['muzuid'] = variable_get('media_muzu__api_key','');
    $options['format'] = 'json';
    $options['id'] = check_plain($id);

    $url = url(MEDIA_MUZU_REST_API . '/video/details', array('query' => $options));

    $response = drupal_http_request($url);
    if (!isset($response->error)) {
      $data = drupal_json_decode($response->data);
      //get biggest thumbnail: thumbnail_2
      return $data[0]['thumbnail_2']['url'];
    }
    else {
      throw new Exception("Error Processing Request. (Error: {$response->code}, {$response->error})");
      return;
    }

  }

  function getLocalThumbnailPath() {
    $parts = $this->get_parameters();

    $local_path = file_default_scheme() . '://media-muzu/' . check_plain($parts['v']) . '.jpg';
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      $response = drupal_http_request($this->getOriginalThumbnailPath());
      if (!isset($response->error)) {
        file_unmanaged_save_data($response->data, $local_path, TRUE);
      }
      else {
        @copy($this->getOriginalThumbnailPath(), $local_path);
      }
    }
    return $local_path;
  }
}
