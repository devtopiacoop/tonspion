<?php
//define('PARQUES_SAMPLE', 0);


/**
 * GMaps Parques settings.
 */
function media_muzu_settings() {
  $form = array();

  $form['#prefix']  = '<h2>Media Muzu settings</h2>';

  $form['media_muzu__api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Muzu API Key for this site'),
    '#description' => t('Please input the API Key for this site'),
    '#default_value' => variable_get('media_muzu__api_key', null),
  );


  return system_settings_form($form);
}