<?php

/**
 * @file media_muzu/includes/MediaInternetMuzuHandler.inc
 *
 * Contains MediaInternetMuzuHandler.
 */

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetMuzuHandler extends MediaInternetBaseHandler {
  /**
   * Check if a Muzu video id is valid.
   *
   * @return
   *   Boolean.
   */
  static public function validId($id) {

    // Check against the oembed stream instead of the gdata api site to
    //avoid "yt:quota too_many_recent_calls" errors.    
    //$url = 'http://www.muzu.tv/api/oembed/?url=http%3A%2F%2Fwww.muzu.tv%2F'.$artist.'%2F'.$track.'%2F'.$id.'%2F&format=xml'

    $options['muzuid'] = variable_get('media_muzu__api_key','');
    $options['format'] = 'xml';
    $options['id'] = $id;

    //Ex: http://www.muzu.tv/api/video/details?muzuid=qrEF3lEvZv&id=2031714&format=xml

    $url = url(MEDIA_MUZU_REST_API . '/video/details', array('query' => $options));
    $response = drupal_http_request($url, array('method' => 'HEAD'));

    if ($response->code == 401) {
      throw new MediaInternetValidationException("Embedding has been disabled for this video.");
    }
    elseif ($response->code != 200) {
      throw new MediaInternetValidationException("The Muzu video ID is invalid or the video was deleted.");
    }
    return TRUE;
  }

  /**
   * Parses Muzu video embed code or vide URL
   *
   * @return
   *   Muzu video URI like muzu://v/[video-id]
   */
  public function parse($embed_code) {

    //Ex: http://player.muzu.tv/player/getPlayer/i/267076/vidId=2120862&la=n

    //Keep patterns in this order as the first ones are more exact
    $patterns = array(
      '@muzu.tv\/player\/getPlayer\/(.+)\/(\d+)\/vidId=(\d+)@i',
      '@muzu.tv\/(.+)\/(.+)\/(\d+)@i'
    );
    foreach ($patterns as $pattern) {
      preg_match($pattern, $embed_code, $matches);
      // @TODO: Parse is called often. Refactor so that valid ID is checked
      // when a video is added, but not every time the embedCode is parsed.
      if (isset($matches[3]) && self::validId($matches[3])) {
        return file_stream_wrapper_uri_normalize('muzu://v/' . $matches[3]);
      }
    }
  }

  /**
   * Returns true if embedCode contains a valid Muzu video Id
   *
   * @return
   *   Boolean
   */
  public function claim($embed_code) {
    if ($this->parse($embed_code)) {
      return TRUE;
    }
  }

  /**
   * Get file object
   *
   * @return
   *   File object
   */
  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);
    if (empty($file->fid) && $info = $this->getVideoDetails($this->embedCode)) {
      $file->filename = truncate_utf8( /*$info[0]['artistname'].' - '.*/ $info[0]['title'], 255);
    }

    return $file;
  }

  /**
   * Returns information about the media in selected fotmat
   *
   * @return
   *  If Video ID exists, we get its details in json
   *    //Ex: http://www.muzu.tv/api/video/details?muzuid=qrEF3lEvZv&id=2031714&format=xml
   *
   *  Instead of Muzu API We could have uses OEmbed
   *   http://www.muzu.tv/api/oembed/?url=http%3A%2F%2Fwww.muzu.tv%2Fdefected%2Fcat-walk-original-mix-music-video%2F1981454%2F&format=json
   */
  public function getVideoDetails( $uri ) {
    $uri = $this->parse($this->embedCode);

    $wrapper = file_stream_wrapper_get_instance_by_uri($uri);
    $parts = $wrapper->get_parameters();

    $options['muzuid'] = variable_get('media_muzu__api_key','');
    $options['format'] = 'json';
    $options['id'] = check_plain($parts['v']);

    $url = url(MEDIA_MUZU_REST_API . '/video/details', array('query' => $options));

    $response = drupal_http_request($url);
    if (!isset($response->error)) {
      return drupal_json_decode($response->data);
    }
    else {
      throw new Exception("Error Processing Request. (Error: {$response->code}, {$response->error})");
      return;
    }
  }
}
