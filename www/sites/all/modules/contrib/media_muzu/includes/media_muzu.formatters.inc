<?php

/**
 * @file media_muzu/includes/media_muzu.formatters.inc
 *
 * Formatters for Media: Muzu.
 * Set up in drupal in /admin/config/media/file-types/manage/video/file-display
 */

/**
 * Implements hook_file_formatter_info().
 */
function media_muzu_file_formatter_info() {

  $formatters['media_muzu_video'] = array(
    'label' => t('Muzu Video'),
    'file types' => array('video'),
    'default settings' => array(),
    'view callback' => 'media_muzu_file_formatter_video_view',
    'settings callback' => 'media_muzu_file_formatter_video_settings',
    'mime types' => array('video/muzu'),
  );

    $formatters['media_muzu_video']['default settings'] = array(
      'width' => 640,
      'height' => 390,
      'autostart' => FALSE,
      'soundoff' => FALSE,
      'protocol' => 'http:',
    );

  $formatters['media_muzu_image'] = array(
    'label' => t('Muzu Preview Image'),
    'file types' => array('video'),
    'default settings' => array(
      'image_style' => '',
    ),
    'view callback' => 'media_muzu_file_formatter_image_view',
    'settings callback' => 'media_muzu_file_formatter_image_settings',
    'mime types' => array('video/muzu'),
  );

  return $formatters;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_muzu_file_formatter_video_view($file, $display, $langcode) {
  
  $scheme = file_uri_scheme($file->uri);
  // WYSIWYG does not yet support video inside a running editor instance.
  if ($scheme == 'muzu' && empty($file->override['wysiwyg'])) {
    $element = array(
      '#theme' => 'media_muzu_video',
      '#uri' => $file->uri,
      '#options' => array(),
    );

    // Fake a default for attributes so the ternary doesn't choke.
    $display['settings']['attributes'] = array();

    foreach ( array(
        'width', 
        'height', 
        'autostart', 
        'soundoff', 
        'protocol', 
      ) as $setting) {
        $element['#options'][$setting] = isset($file->override[$setting]) ? $file->override[$setting] : $display['settings'][$setting];
    }
    return $element;
  }
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 *
 * @see http://www.muzu.tv/api/playerDoc/
 */
function media_muzu_file_formatter_video_settings($form, &$form_state, $settings) {
  $element = array();

  $element['width'] = array(
    '#title' => t('Width'),
    '#type' => 'textfield',
    '#default_value' => $settings['width'],
    '#element_validate' => array('_muzu_validate_video_width_and_height'),
  );
  $element['height'] = array(
    '#title' => t('Height'),
    '#type' => 'textfield',
    '#default_value' => $settings['height'],
    '#element_validate' => array('_muzu_validate_video_width_and_height'),
  );

  //Single Options.
  $element['autostart'] = array(
    '#title' => t('Autoplay video on load'),
    '#type' => 'checkbox',
    '#default_value' => $settings['autostart'],
  );
  $element['soundoff'] = array(
    '#title' => t('Sound off video'),
    '#type' => 'checkbox',
    '#default_value' => $settings['soundoff'],
  );
  $element['protocol'] = array(
    '#title' => t('Iframe protocol'),
    '#type' => 'radios',
    '#default_value' => $settings['protocol'],
    '#options' => array(
      'http:' => 'http://',
      'https:' => 'https://',
    ),
    '#states' => array(
      'invisible' => array(
        ':input[name="displays[media_muzu_video][settings][protocol_specify]"]' => array('checked' => FALSE),
      ),
    ),
  );

  return $element;
}


/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_muzu_file_formatter_image_view($file, $display, $langcode) {

  $scheme = file_uri_scheme($file->uri);
  
  if ($scheme == 'muzu') {

    $wrapper = file_stream_wrapper_get_instance_by_uri($file->uri);
    $image_style = $display['settings']['image_style'];
    $valid_image_styles = image_style_options(FALSE);

    // @TODO: If autosubmit is removed and we allow view modes that insert
    // images in the WYSIWYG, add file->overrides handling.
    if (empty($image_style) || !isset($valid_image_styles[$image_style])) {
      $element = array(
        '#theme' => 'image',
        '#path' => $wrapper->getOriginalThumbnailPath(),
        '#alt' => isset($file->override['attributes']['alt']) ? $file->override['attributes']['alt'] : $file->filename,
      );
    }
    else {
      $element = array(
        '#theme' => 'image_style',
        '#style_name' => $image_style,
        '#path' => $wrapper->getLocalThumbnailPath(),
        '#alt' => isset($file->override['attributes']['alt']) ? $file->override['attributes']['alt'] : $file->filename,
      );
    }

    return $element;
  }

}



/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_muzu_file_formatter_image_settings($form, &$form_state, $settings) {
  $element = array();
  $element['image_style'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#options' => image_style_options(FALSE),
    '#default_value' => $settings['image_style'],
    '#empty_option' => t('None (original image)'),
  );
  return $element;
}

/**
 * Implements hook_file_default_displays_alter().
 */
function media_muzu_file_default_displays_alter(&$file_displays) {

  //Display: video default
  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__default__media_muzu_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '640',
    'height' => '390',
    'autostart' => 0,
    'soundoff' => 0,
    'protocol' => 'http:',
  );
  $file_displays['video__default__media_muzu_video'] = $file_display;


  //Display: video image
  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__preview__media_muzu_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'media_thumbnail',
  );
  $file_displays['video__preview__media_muzu_image'] = $file_display;


  //Display: video in teaser
  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__teaser__media_muzu_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '280',
    'height' => '170',
    'autostart' => 0,
    'soundoff' => 0,
    'protocol' => 'http:',
  );
  $file_displays['video__teaser__media_muzu_video'] = $file_display;
}


/**
 * Validation for width and height.
 */
function _muzu_validate_video_width_and_height($element, &$form_state, $form) {

  // Check if the value is a number with an optional decimal or percentage sign, or "auto".
  if (!empty($element['#value']) && !preg_match('/^(auto|([0-9]*(\.[0-9]+)?%?))$/', $element['#value'])) {
    form_error($element, t("The value entered for @dimension is invalid. Please insert a unitless integer for pixels, a percent, or \"auto\". Note that percent and auto may not function correctly depending on the browser and doctype.", array('@dimension' => $element['#title'])));
  }
}