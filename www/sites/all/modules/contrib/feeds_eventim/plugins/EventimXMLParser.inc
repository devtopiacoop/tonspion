<?php

/**
 * Class definition for Common Syndication Parser.
 *
 * Parses RSS and Atom feeds.
 */
class EventimXMLParser extends FeedsParser {

  /**
   * Implements FeedsParser::parse().
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) 
  {
    $source_config = $source->getConfigFor($this);
    $state = $source->state(FEEDS_PARSE);

    // We use XMLReader parser as Eventim files are very large (aprox 100MB)
    $xmls = $fetcher_result->getXMLPaths();

    if(!empty($xmls)){

      feeds_include_library('EventimXMLReader.inc', 'EventimXMLReader');

      // We only process first XML file path as Eventim only provides one file
      $xml_path = drupal_realpath($xmls[0]);
      $parser = new EventimXMLReader();

      // Count all eventserie elements
      if(empty($state->total) || $state->total==0){
        $parser->open($xml_path);
        $this->getItemsTotal($parser);
        $state->total = $parser->lastEventSeriePos();
        $parser->close();
      }

      // Parse items, reopen to rewind pointer
      $parser->open($xml_path);
      $parser->rewind();

      // Determine items to parse.
      $rows = array();
      $start = $state->pointer ? $state->pointer : $parser->lastEventSeriePos();
      $limit = $source->importer->getLimit();

      $rows = $this->parseItems($parser, $start, $limit);

      // Report progress.
      $state->pointer = $parser->lastEventSeriePos();
      $progress = $parser->lastEventSeriePos() ? $parser->lastEventSeriePos() : $state->total;
      $state->progress($state->total, $progress);
      $parser->close();

      //dsm($state);

    }

    return new FeedsParserResult($rows, $source->feed_nid);
  }



/**
 * Parse the XML into a data structure. 
 * It uses XMLReader to get eventseries and SimpleXML to parse them, saving memory.
 *
 * @param $parser
 *  Parser object
 * @return
 *  stdClass The structured data extracted from the XML
 */
  private function getItemsTotal($parser)
  {

    $counter = 0;
    while ($parser->readToNextEventSerie()) {
      $counter++;
    }

    return $counter;
  }

/**
 * Parse the XML into a data structure. 
 * It uses XMLReader to get eventseries and SimpleXML to parse them, saving memory.
 *
 * @param $parser
 *  Parser object
 * @return
 *  stdClass The structured data extracted from the XML
 */
  private function parseItems($parser, $start = 0, $limit = 0)
  {

    $events = array();

    // Use XMLReader to consume less memory, as it is liberated in each iteration
    $items_count = 0;
    $parser->moveToEventSerie($start);
    while ($parser->readToNextEventSerie()) {

      // Use SimpleXMLElement for each serie as it is easier
      $new_events = $this->parseEventimEventSeries( new SimpleXMLElement($parser->readOuterXML()) );
      $events = array_merge($events, $new_events);
      $items_count++;

      // Check items limit
      if($limit && $items_count >= $limit ){
        break;
      }      
    }

    //dsm($events);

    return $events;
  }

/**
 * Parse the XML into a data structure.
 *
 * @param $xml
 *  The XML object
 * @return
 *  stdClass The structured data extracted from the XML
 */
  private function parseEventimEventSeries($tour_node)
  {

    $events = array();

    //Tour
    $tour = array(
        'eventserie:id'             => (int) $tour_node->esid,
        'eventserie:name'           => (string) $tour_node->esname,
        'eventserie:text'           => (string) $tour_node->estext,
        'eventserie:picture_small'  => (string) $tour_node->espicture_small,
        'eventserie:picture'        => (string) $tour_node->espicture,
        'eventserie:picture_big'    => (string) $tour_node->espicture_big,
        'eventserie:begin'          => (string) $tour_node->esbegin,
        'eventserie:end'            => (string) $tour_node->esend,
        'eventserie:categories'     => (string) $tour_node->escategorie,
        'eventserie:topten'         => (string) $tour_node->topten,
        'eventserie:ranking'        => (string) $tour_node->ranking,
        'eventserie:artist:id'      => (int) $tour_node->artist->artistid,
        'eventserie:artist:name'    => (string) $tour_node->artist->artistname
    );

    //Concerts in tour
    foreach ($tour_node->xpath('event') as $concert_node)
    {
        $event = array(
            'event:id'            => (int) $concert_node->eventid,
            'event:name'          => (string) $concert_node->eventname,
            'event:link'          => (string) $concert_node->eventlink,
            'event:datetime'      => (string) $concert_node->eventdate . ' ' .(string) $concert_node->eventtime . ':00',
            'event:place:id'      => (string) $concert_node->eventplaceid,
            'event:place:name'    => ucwords(strtolower($concert_node->eventplace)),
            'event:venue'         => (string) $concert_node->eventvenue,
            'event:zip'           => (int) $concert_node->eventzip,
            'event:street'        => (string) $concert_node->eventstreet,
            'event:city'          => (string) $concert_node->eventplace,
            'event:country'       => (string) $concert_node->eventcountry,
            'event:promoter:id'   => (int) $concert_node->promoterid,
            'event:ticketdirect'  => isset($concert_node->ticketdirect),
            'event:willcall'      => isset($concert_node->willcall),
            'event:deliverable'   => isset($concert_node->deliverable),
            'event:price'         => $this->_getPrice( $concert_node->xpath('pricekategory' ))
        ) + $tour;

        $events[] = $event;
    }

    return $events;
  }




/**
 * Parse the XML into a data structure.
 *
 * @param $xml
 *  The XML object
 * @return
 *  stdClass The structured data extracted from the XML
 */
  private function _parseEventimXML_SimpleXML($xml)
  {

    $events = array();
    $events_series = $xml->xpath("eventserie");

    foreach($xml->children() as $tour_node)
    {
        $events = array_merge($evens, _parseEventimEventSeries( $tour_node) );
    }

    return $events;
  }


/**
 * Get price from XML
 *
 * @param $price_categories
 *  price categories XML
 * @return
 *  int Prices as a string
 */
  private function _getPrice(array $price_categories)
  {
      $text = '';
      $price = 0;
      $currency = '';

      foreach($price_categories as $price_category)
      {
          if ($price == 0 || (float) $price > (float) $price_category->price)
          {
              $text = $price_category->pctext;
              $price = $price_category->price;
              $currency = $price_category->currency;
          }
      }

      // if (count($price_categories) > 1)
      // {
      //     $price = 'AB ' . $price;
      // }

      return  t( '!text: !currency !price', array('!text'=>$text, '!price'=>$price, '!currency'=>$currency) );
  }




  /**
   * Return mapping sources.
   *
   * At a future point, we could expose data type information here,
   * storage systems like Data module could use this information to store
   * parsed data automatically in fields with a correct field type.
   */
  public function getMappingSources() {

    return array(

      //Event Serie source mappers
      'eventserie:id' => array(
        'name' => t('Event Serie ID'),
        'description' => t('Unique ID of the event series')
      ),
      'eventserie:name' => array(
        'name' => t('Event Serie Name'),
        'description' => t('Identification of the event series')
      ),
      'eventserie:link' => array(
        'name' => t('Event Serie Link'),
        'description' => t('Link to the event series')
      ),
      'eventserie:text' => array(
        'name' => t('Event Serie Text'),
        'description' => t('Short description of the event series')
      ),
      'eventserie:picture_small' => array(
        'name' => t('Event Serie Picture small'),
        'description' => t('URL of the small (60x60 px) picture to the event series')
      ),
      'eventserie:picture' => array(
        'name' => t('Event Serie Picture'),
        'description' => t('URL of the medium (100x100 px) picture to the event series')
      ),
      'eventserie:picture_big' => array(
        'name' => t('Event Serie Picture big'),
        'description' => t('URL of the big (222x222 px) picture to the event series')
      ),
      'eventserie:begin' => array(
        'name' => t('Event Serie begin date'),
        'description' => t('Date of the first event of the event series')
      ),
      'eventserie:end' => array(
        'name' => t('Event Serie end date'),
        'description' => t('Date of the last event of the event series')
      ),
      'eventserie:categories' => array(
        'name' => t('Event Serie categories'),
        'description' => t('event category to which the event series is assigned')
      ),
      'eventserie:topten' => array(
        'name' => t('Event Serie topten'),
        'description' => t('If this event is considered to be a "top 10" event then this field will be 1. Any other value indicates an event not in the top 10')
      ),
      'eventserie:ranking' => array(
        'name' => t('Event Serie ranking'),
        'description' => t('ranking of events (1-x). Is the particular event not available in the ranking, then ranking = 0')
      ),
      'eventserie:artist:id' => array(
        'name' => t('Event Serie Artist ID'),
        'description' => t('Unique ID of the artist')
      ),
      'eventserie:artist:name' => array(
        'name' => t('Event Serie Artist name'),
        'description' => t('name of the artist')
      ),


      //Event source mappers
      'event:id' => array(
        'name' => t('Event ID'),
        'description' => t('unique ID of the event')
      ),
      'event:name' => array(
        'name' => t('Event name'),
        'description' => t('name of the event')
      ),
      'event:onsaledate' => array(
        'name' => t('Event on sale date'),
        'description' => t('date on which the event goes on sale.')
      ),
      'event:onsaletime' => array(
        'name' => t('Event on sale time'),
        'description' => t('time on the onsaledate at which the event goes on sale (format hh:mm)')
      ),
      'event:place:id' => array(
        'name' => t('Event place ID'),
        'description' => t('Third party key to event location (not used by most)')
      ),
      'event:place:name' => array(
        'name' => t('Event place name'),
        'description' => t('city where the event takes place')
      ),
      'event:datetime' => array(
        'name' => t('Event datetime'),
        'description' => t('date of the event')
      ),
      // 'event:date' => array(
      //   'name' => t('Event time'),
      //   'description' => t('date of the event')
      // ),
      // 'event:time' => array(
      //   'name' => t('Event time'),
      //   'description' => t('date of the event (format hh:mm)')
      // ),
      'event:link' => array(
        'name' => t('Event link'),
        'description' => t('link to the event')
      ),
      'event:street' => array(
        'name' => t('Event street'),
        'description' => t('street of the venue (sometimes contains the house number)')
      ),
      'event:zip' => array(
        'name' => t('Event zip'),
        'description' => t('Postcode of the venue')
      ),
      'event:venue' => array(
        'name' => t('Event venue'),
        'description' => t('name of the venue')
      ),
      'event:country' => array(
        'name' => t('Event country'),
        'description' => t('Venue country (see ISO 3166-1 abbreviations e.g. DE for Germany)')
      ),
      'event:promoter:id' => array(
        'name' => t('Event promoter ID'),
        'description' => t('unique ID of the promoter')
      ),
      'event:ticketdirect ' => array(
        'name' => t('Event ticket direct'),
        'description' => t('If element exists then customers can print their own tickets.')
      ),
      'event:willcall ' => array(
        'name' => t('Event will call '),
        'description' => t('If element exists then customers can collect their tickets from the venue.')
      ),
      'event:deliverable' => array(
        'name' => t('Event deliverable'),
        'description' => t('If true then tickets can still be delivered for this event. If false then delivery (and therefore sales) are not possible.')
      ),
      'event:price' => array(
        'name' => t('Event Tickets prices'),
        'description' => t('Ticket price values as a text field')
      ),
      // 'event:price:value' => array(
      //   'name' => t('Ticket face value'),
      //   'description' => t('Ticket face value')
      // ),
      // 'event:price:currency' => array(
      //   'name' => t('Event price currency'),
      //   'description' => t('Currency identification')
      // ),
      // 'event:price:text' => array(
      //   'name' => t('Event price text'),
      //   'description' => t('Further description of the price category')
      // ),

    ) + parent::getMappingSources();
  }


// /**
//  * Parse the XML into a data structure.
//  *
//  * @param $xml
//  *  The XML object
//  * @return
//  *  stdClass The structured data extracted from the XML
//  */
//   private function _parseEventim_XMLReader_BAK($reader)
//   {

//     $events = array();

//     // while: eventseries
//     while ($reader->readToNext('eventserie')) {
//       $tour = array();
//       $depth = $reader->depth;

//       // while: eventserie elements
//       while ($reader->readToNextChildElement($depth)) {

//         // switch: eventserie
//         switch ($reader->localName) {
//           case 'esid':
//             $tour['eventserie:id']           =    (int) $reader->getNodeValue(); 
//             break;
//           case 'esname': 
//             $tour['eventserie:name']         = (string) $reader->getNodeValue(); 
//             break;
//           case 'estext': 
//             $tour['eventserie:text']         = (string) $reader->getNodeValue(); break;
//           case 'espicture_small': 
//             $tour['eventserie:image_small']  = (string) $reader->getNodeValue(); 
//             break;
//           case 'espicture': 
//             $tour['eventserie:image']        = (string) $reader->getNodeValue(); 
//             break;
//           case 'espicture_big': 
//             $tour['eventserie:image_big']    = (string) $reader->getNodeValue(); 
//             break;
//           case 'esbegin': 
//             $tour['eventserie:begin']        = (string) $reader->getNodeValue(); 
//             break;
//           case 'esend': 
//             $tour['eventserie:end']          = (string) $reader->getNodeValue(); 
//             break;
//           case 'escategorie': 
//             $tour['eventserie:categories']   = (string) $reader->getNodeValue(); 
//             break;
//           case 'topten': 
//             $tour['eventserie:topten']       = (string) $reader->getNodeValue(); 
//             break;
//           case 'ranking': 
//             $tour['eventserie:ranking']      = (string) $reader->getNodeValue(); 
//             break;
//           case 'artistid': 
//             $tour['eventserie:artist:id']    =    (int) $reader->getNodeValue(); 
//             break;
//           case 'artistname': 
//             $tour['eventserie:artist:name']  = (string) $reader->getNodeValue(); 
//             break;

//           case 'event':
//             $event = array();
//             //Deafault values
//             $event_time = $event_date = null;
//             $event['event:ticketdirect'] = $event['event:willcall'] = $event['event:deliverable'] = false;
//             $event_prices = array();
            
//             //$event_data = $reader->readEvent($depth);

//             // while: event elements
//             while ($reader->readToNextChildElement($depth)) {

//               // switch:  event elements
//               switch ($reader->localName) {
//                 case 'eventid':
//                   $event['event:id']            =       (int) $reader->getNodeValue(); 
//                   break;
//                 case 'eventname':
//                   $event['event:name']          =    (string) $reader->getNodeValue(); 
//                   break;
//                 case 'eventlink':
//                   $event['event:link']          =    (string) $reader->getNodeValue(); 
//                   break;
//                 case 'eventime':
//                   $event_time                   =    (string) $reader->getNodeValue(); 
//                   break;
//                 case 'evendate':
//                   $event_date                   =    (string) $reader->getNodeValue(); 
//                   break;
//                 case 'eventplaceid':
//                   $event['event:place:id']      =    (string) $reader->getNodeValue(); 
//                   break;

//                 case 'eventplace':
//                   $event['event:place:name']    =    ucwords(strtolower( $reader->getNodeValue() ));
//                   $event['event:city']          =    (string) $reader->getNodeValue(); 
//                   break;
//                 case 'eventvenue':
//                   $event['event:venue']         =    (string) $reader->getNodeValue(); 
//                   break;
//                 case 'eventzip':
//                   $event['event:zip']           =       (int) $reader->getNodeValue(); 
//                   break;
//                 case 'eventstreet':
//                   $event['event:street']        =    (string) $reader->getNodeValue(); 
//                   break;

//                 case 'eventcountry':
//                   $event['event:country']       =    (string) $reader->getNodeValue(); 
//                   break;
//                 case 'promoterid':
//                   $event['event:promoter:id']   =       (int) $reader->getNodeValue(); 
//                   break;
//                 case 'ticketdirect':
//                   $event['event:ticketdirect']  =    true; 
//                   break;
//                 case 'willcall':
//                   $event['event:willcall']      =    true; 
//                   break;
//                 case 'deliverable':
//                   $event['event:deliverable']   =    true; 
//                   break;

//                 // Prices
//                 case 'pricekategory':
//                   //Read event values
//                   $p = array();

//                   // while: price elements
//                   while ($reader->readToNextChildElement($depth)) {
//                     switch ($reader->localName) {
//                       case 'pctext':    $p['pctext']   = (string) $reader->getNodeValue(); 
//                         break;
//                       case 'price':     $p['price']    = (string) $reader->getNodeValue(); 
//                         break;
//                       case 'currency':  $p['currency'] = (string) $reader->getNodeValue(); 
//                         break;
//                     }
//                   } // while: price elements - END

//                   $event_prices[] = t( '!text: !currency !price', 
//                     array(
//                       '!text'=>$p['pctext'], 
//                       '!price'=>$p['price'], 
//                       '!currency'=>$p['currency']
//                     ) 
//                   );

//                   break; // break: pricekategory
                  
//               } // switch:  event elements - END
            
//             } //while: event elements - END

//             // Composed values
//             $event['event:datetime']  = $event_date .' '. $event_time . ':00';
//             $event['event:price']     = implode('<br/>', $event_prices);

//             //Adds new event to array
//             $events[] = $event + $tour;

//             break; // break: event

//         } //switch: event elements - END

//       } // while: eventserie elements - END

//     } // while: eventseries - END

//     dsm($events);
//     return $events;

//   }




}