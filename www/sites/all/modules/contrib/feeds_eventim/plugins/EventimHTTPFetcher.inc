<?php

/**
 * @file
 * Home of the EventimHTTPFetcher and related classes.
 */

feeds_include_library('PuSHSubscriber.inc', 'PuSHSubscriber');

define('EVENTIM_FILES_DIRECTORY', 'public://eventim/');
define('EVENTIM_FILES_DIRECTORY_SOURCES', 'temporary://');
define('EVENTIM_FILES_DIRECTORY_XML', 'public://eventim/xml/');

/**
 * Result of EventimHTTPFetcher::fetch().
 */
class EventimHTTPFetcherResult extends FeedsHTTPFetcherResult {
  protected $url;
  protected $file_path;
  protected $timeout;
  protected $login;
  protected $password;

  /**
   * Constructor.
   */
  public function __construct($config = array()) {
    //dsm($config);
    $this->login    = $config['source_user'];
    $this->password = $config['source_pwd'];
    parent::__construct($config['source']);
  }


  /**
   * Checks if file is compressed with zip and decompress it. If not compressed returns file path
   * 
   * @return array path to XML files
   */
  public function getXMLPaths() {

    //dsm($this->url);
    //dsm($this->login);
    //dsm($this->password);

    $paths = array();

    feeds_include_library('http_request.inc', 'http_request');
    $result = http_request_get($this->url, $this->login, $this->password, NULL, $this->timeout);

    //Check if we go the proper zip headers.
    //dsm($result->headers, 'headers');
    //dsm($result->headers['content-type'], 'content-type');
    $compressed = false;
    if(strpos($result->headers['content-type'], 'application/zip')!==false ||
       strpos($result->headers['content-type'], 'application/octet-stream')!==false && strpos($result->headers['content-type'], '.zip')!==false){
        $ext = 'zip';
        $compressed = true;
    }

    // Eventim folders
    $dir=EVENTIM_FILES_DIRECTORY;
    file_prepare_directory($dir, FILE_CREATE_DIRECTORY);
    //$dir=EVENTIM_FILES_DIRECTORY_SOURCES;
    //file_prepare_directory($dir, FILE_CREATE_DIRECTORY);
    $dir=EVENTIM_FILES_DIRECTORY_XML;
    file_prepare_directory($dir, FILE_CREATE_DIRECTORY);

    if($compressed){
        if(!function_exists('zip_open')){
            log('Zipped file encoding detected, but you dont have PHP with <strong>php_zlib</strong> or <strong>php_zip</strong> support. You file has <em>'.$ext.'</em> extension.','warning');
        } else {
            $tmp_filename_ = 'eventim_'.date('Ymd\This').'.'. $ext;
            //dsm(EVENTIM_FILES_DIRECTORY_SOURCES.'/'.$tmp_filename_);

            //Save Eventim file                        
            $fsd = file_save_data($result->data, EVENTIM_FILES_DIRECTORY_SOURCES.$tmp_filename_, FILE_EXISTS_REPLACE);
            //dsm($fsd);
            if($fsd && $ext=='zip'){
              $zip = new ZipArchive;
              if ($zip->open(drupal_realpath($fsd->uri)) === TRUE) {
                // consider zip file opened successfully
                for($i = 0; $i < $zip->numFiles; $i++) {
                    $filename[$i] = $zip->getNameIndex($i);
                    $paths[$i] = EVENTIM_FILES_DIRECTORY_XML.$filename[$i];
                    //$fileinfo = pathinfo($filename[$i]);
                    //dsm("zip://".drupal_realpath($fsd->uri)."#".$filename[$i]);
                    copy("zip://".drupal_realpath($fsd->uri)."#".$filename[$i], drupal_realpath($paths[$i]));
                } 
                $zip->close();
                log('Zip encoding detected. Decoding the file to xml.');
              }else{
                drupal_set_message( $this->zipFileErrMsg($fsd) );
              }

            }
            //Delete the temporary file.
            //@unlink(drupal_realpath('public://'.$tmp_filename_));
            //$result->data = trim($contents);
            //dsm($result->data);
        }
    
    } else {
      // Not compressed
      $tmp_filename_ = md5(time()).'.'.$ext;
      //dsm($tmp_filename_);
      //Save Eventim file                        
      $fsd = file_save_data($result->data, EVENTIM_FILES_DIRECTORY_SOURCES.$tmp_filename_, FILE_EXISTS_REPLACE);
      $paths[] = $fsd->uri; 
    }

    // Check result codes
    if (!in_array($result->code, array(200, 201, 202, 203, 204, 205, 206))) {
      throw new Exception(t('Download of @url failed with code !code.', array('@url' => $this->url, '!code' => $result->code)));
    }


    //dsm($paths);
    return $paths;

  }



  /**
   * Overrides FeedsFetcherResult::getRaw();
   */
  public function getRaw() {

    //dsm($this->url);
    //dsm($this->login);
    //dsm($this->password);

    feeds_include_library('http_request.inc', 'http_request');
    $result = http_request_get($this->url, $this->login, $this->password, NULL, $this->timeout);

    //Check if we go the proper zip or gzip headers.
    //dsm($result->headers, 'headers');
    //dsm($result->headers['content-type'], 'content-type');

    $compressed = false;
    if(strpos($result->headers['content-type'], 'application/zip')!==false ||
       strpos($result->headers['content-type'], 'application/octet-stream')!==false && strpos($result->headers['content-type'], '.zip')!==false){
        $ext = 'zip';
        $compressed = true;
    }
    if(strpos($result->headers['content-type'], 'application/x-gzip')!==false ||
       strpos($result->headers['content-type'], 'application/octet-stream')!==false && strpos($result->headers['content-type'], '.gz')!==false){
        $ext = 'gz';
        $compressed = true;
    }

    if($compressed){
        if(!function_exists('gzopen') || !function_exists('zip_open')){
            drupal_set_message('Zipped file encoding detected, but you dont have PHP with <strong>php_zlib</strong> or <strong>php_zip</strong> support. You file has <em>'.$ext.'</em> extension.','warning');
        } else {
            $tmp_filename_ = md5(time()).'.'.$ext;
            //dsm($tmp_filename_);
            $fsd = file_save_data($result->data, 'public://'.$tmp_filename_, FILE_EXISTS_REPLACE);
            //dsm($fsd);
            if($fsd && $ext=='gz'){
                $zd = gzopen(drupal_realpath($fsd->uri), 'r');
                while (!gzeof($zd)) {
                    $contents .= gzread($zd, 10000);
                }
                gzclose($zd);
                drupal_set_message('Gzip encoding detected. Decoding the file to xml.');
            }
            if($fsd && $ext=='zip'){
                $zd = zip_open(drupal_realpath($fsd->uri));
                //dsm($zd);
                if (is_resource($zd)) {
                  // consider zip file opened successfully
                  while ($zip_entry = zip_read($zd)) {
                    //dsm($zip_entry);
                    if (zip_entry_open($zd, $zip_entry, 'r')) {
                        $contents .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                        zip_entry_close($zip_entry);
                    }
                  }
                  zip_close($zd);
                  drupal_set_message('Zip encoding detected. Decoding the file to xml.');
                }else{
                  $contents = $this->zipFileErrMsg($fsd);
                }

            }
            //Delete the temporary file.
            //@unlink(drupal_realpath('public://'.$tmp_filename_));
            $result->data = trim($contents);
            //dsm($result->data);
        }
    }

    // Check result codes
    if (!in_array($result->code, array(200, 201, 202, 203, 204, 205, 206))) {
      throw new Exception(t('Download of @url failed with code !code.', array('@url' => $this->url, '!code' => $result->code)));
    }


    return $this->sanitizeRaw($result->data);
  }


  private function zipFileErrMsg($errno) {
    // using constant name as a string to make this function PHP4 compatible
    $zipFileFunctionsErrors = array(
      'ZIPARCHIVE::ER_MULTIDISK' => 'Multi-disk zip archives not supported.',
      'ZIPARCHIVE::ER_RENAME' => 'Renaming temporary file failed.',
      'ZIPARCHIVE::ER_CLOSE' => 'Closing zip archive failed', 
      'ZIPARCHIVE::ER_SEEK' => 'Seek error',
      'ZIPARCHIVE::ER_READ' => 'Read error',
      'ZIPARCHIVE::ER_WRITE' => 'Write error',
      'ZIPARCHIVE::ER_CRC' => 'CRC error',
      'ZIPARCHIVE::ER_ZIPCLOSED' => 'Containing zip archive was closed',
      'ZIPARCHIVE::ER_NOENT' => 'No such file.',
      'ZIPARCHIVE::ER_EXISTS' => 'File already exists',
      'ZIPARCHIVE::ER_OPEN' => 'Can\'t open file', 
      'ZIPARCHIVE::ER_TMPOPEN' => 'Failure to create temporary file.', 
      'ZIPARCHIVE::ER_ZLIB' => 'Zlib error',
      'ZIPARCHIVE::ER_MEMORY' => 'Memory allocation failure', 
      'ZIPARCHIVE::ER_CHANGED' => 'Entry has been changed',
      'ZIPARCHIVE::ER_COMPNOTSUPP' => 'Compression method not supported.', 
      'ZIPARCHIVE::ER_EOF' => 'Premature EOF',
      'ZIPARCHIVE::ER_INVAL' => 'Invalid argument',
      'ZIPARCHIVE::ER_NOZIP' => 'Not a zip archive',
      'ZIPARCHIVE::ER_INTERNAL' => 'Internal error',
      'ZIPARCHIVE::ER_INCONS' => 'Zip archive inconsistent', 
      'ZIPARCHIVE::ER_REMOVE' => 'Can\'t remove file',
      'ZIPARCHIVE::ER_DELETED' => 'Entry has been deleted',
    );
    $errmsg = 'unknown';
    foreach ($zipFileFunctionsErrors as $constName => $errorMessage) {
      if (defined($constName) and constant($constName) === $errno) {
        return 'Zip File Function error: '.$errorMessage;
      }
    }
    return 'Zip File Function error: unknown';
  }  

}

/**
 * Fetches data via HTTP.
 */
class EventimHTTPFetcher extends FeedsHTTPFetcher {


  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    if ($this->config['use_pubsubhubbub'] && ($raw = $this->subscriber($source->feed_nid)->receive())) {
      return new FeedsFetcherResult($raw);
    }
    $fetcher_result = new EventimHTTPFetcherResult($source_config);
    // When request_timeout is empty, the global value is used.
    $fetcher_result->setTimeout($this->config['request_timeout']);
    return $fetcher_result;
  }

  /**
   * Expose source form.
   */
  public function sourceForm($source_config) {
    $form = array();
    $form['source'] = array(
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#description' => t('Enter an Eventim feed URL: http://feeds.eventim.com/export/users/[username]/latest.zip'),
      '#default_value' => isset($source_config['source']) ? $source_config['source'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );
    $form['source_user'] = array(
      '#type' => 'textfield',
      '#title' => t('User'),
      '#description' => t('Enter an Eventim user.'),
      '#default_value' => isset($source_config['source_user']) ? $source_config['source_user'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );
    $form['source_pwd'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#description' => t('Enter an Eventim user password.'),
      '#default_value' => isset($source_config['source_pwd']) ? $source_config['source_pwd'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );          
    return $form;
  }

  /**
   * Convenience method for instantiating a subscriber object.
   */
  protected function subscriber($subscriber_id) {
    return PushSubscriber::instance($this->id, $subscriber_id, 'PuSHSubscription', EventimPuSHEnvironment::instance());
  }
}


/**
 * Provide environmental functions to the PuSHSubscriber library.
 */
class EventimPuSHEnvironment implements PuSHSubscriberEnvironmentInterface {
  /**
   * Singleton.
   */
  public static function instance() {
    static $env;
    if (empty($env)) {
      $env = new EventimPuSHEnvironment();
    }
    return $env;
  }

  /**
   * Implements PuSHSubscriberEnvironmentInterface::msg().
   */
  public function msg($msg, $level = 'status') {
    drupal_set_message(check_plain($msg), $level);
  }

  /**
   * Implements PuSHSubscriberEnvironmentInterface::log().
   */
  public function log($msg, $level = 'status') {
    switch ($level) {
      case 'error':
        $severity = WATCHDOG_ERROR;
        break;
      case 'warning':
        $severity = WATCHDOG_WARNING;
        break;
      default:
        $severity = WATCHDOG_NOTICE;
        break;
    }
    feeds_dbg($msg);
    watchdog('EventimHTTPFetcher', $msg, array(), $severity);
  }
}
