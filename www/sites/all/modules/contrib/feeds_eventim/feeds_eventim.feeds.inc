<?php

/**
 * @file
 * Feeds Eventim - basic Feeds functions and hook implementations.
 */


/**
 * Implements hook_feeds_plugins().
 */
function feeds_eventim_feeds_plugins() {

  $path = drupal_get_path('module', 'feeds_eventim') . '/plugins';
  $info = array();

  $info['EventimHTTPFetcher'] = array(
    'name' => 'Eventim HTTP Fetcher',
    'description' => 'Download content from Eventim.',
    'handler' => array(
      'parent' => 'FeedsFetcher', // This is the key name, not the class name.
      'class' => 'EventimHTTPFetcher',
      'file' => 'EventimHTTPFetcher.inc',
      'path' => $path,
    ),
  );

  if (extension_loaded('XMLReader')) {
    $info['EventimXMLParser'] = array(
      'name' => 'Eventim XML parser',
      'description' => 'Parse Eventim XML. Extended from common FeedsParser parser.',
      'help' => 'Parse events from Eventim XML.',
      'handler' => array(
        'parent' => 'FeedsParser',  // This is the key name, not the class name.
        'class' => 'EventimXMLParser',
        'file' => 'EventimXMLParser.inc',
        'path' => $path,
      ),
    );
  }

  return $info;
}
