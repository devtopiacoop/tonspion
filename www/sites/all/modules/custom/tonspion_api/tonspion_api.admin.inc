<?php
//define('PARQUES_SAMPLE', 0);

/**
 * Tonspion API settings.
 */
function tonspion_api_settings() {

  $form = array();

  $form['#prefix']  = '<h2>'.t('Tonspion API settings').'</h2>';

  $form['tonspion_api__url'] = array(
    '#type' => 'textfield',
    '#title' => t('API base path'),
    '#description' => t('Please input API access point.'),
    '#default_value' => variable_get('tonspion_api__url', TONSPION_API_URL),
  );

  return system_settings_form($form);
}

