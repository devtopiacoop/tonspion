<?php

/**
 * Tonspion API helper function.
 */
function tonspion_api_get_artist( $id ) {

  $artist['news'] = tonspion_api_get_artist_news( $id );
  $artist['albums'] = tonspion_api_get_artist_albums( $id );
  $artist['mp3s'] = tonspion_api_get_artist_mp3s( $id );
  $artist['videos'] = tonspion_api_get_artist_videos( $id );
  $artist['events'] = tonspion_api_get_artist_events( $id );
  $artist['related'] = tonspion_api_get_artist_related( $id );

  return array( $id => $artist );
}


/**
 * Tonspion API helper function.
 */
function tonspion_api_get_artist_news( $nid, $limit = null, $from_date = null ) {

  $nids = tonspion_artist_news( $nid );
  $news_items = entity_load('node', $nids);
  $from_date = (!empty($from_date)) ? $from_date : strToTime('-1 year'); // 3 months by default
  $counter = 0;
  $news = array();

  //Map properties
  foreach ($news_items as $key => $record) {

    // Check number of items limit
    if( !empty($limit) && $counter >= $limit ){
      break;
    } 

    // Add item if its date is bigger than $from_date
    if( $record->created >= $from_date){

      //Headline
  	  $field_name='field_secondheadline';
  	  $headlines = field_get_items('node', $record, $field_name);
  	  if (!empty($headlines)){
  	    $headline = field_view_value('node', $record, $field_name, $headlines[0] );
  	  }

  	  //Image
  	  $field_name='field_news_image';
  	  $images = field_get_items('node', $record, $field_name);
  	  if (!empty($images)){
  	    $image = field_view_value('node', $record, $field_name, $images[0] );
  	  }	  

    	$item['id'] 				= $key;
    	$item['title'] 			= $record->title;
    	$item['headline'] 	= $headline['#markup'];
    	$item['created'] 		= $record->created;
    	$item['changed'] 		= $record->changed;
    	$item['image_url'] 	= image_style_url('medium', $image['#item']['uri']);
    	$item['url'] 				= drupal_get_path_alias('node/'.$key);

    	$news[] = $item;
      $counter++;
    }

  }


  return $news;

}


/**
 * Tonspion API helper function.
 */
function tonspion_api_get_artist_events( $nid, $country = null, $city = null, $limit = null, $from_date = null  ) {

  $nids = tonspion_artist_events( $nid ); // We not pass here country and city as returned array is cached
  $event_items = entity_load('node', $nids);
  $from_date = (!empty($from_date)) ? $from_date : strToTime('-1 year'); // 3 months by default
  $counter = 0;
  $events = array();

  //Map properties
  foreach ($event_items as $key => $record) {

    // Check number of items limit
    if( !empty($limit) && $counter >= $limit ){
      break;
    } 

    // Add item if its date is bigger than $from_date
    if( $record->created >= $from_date){

  	  //Location
  	  $field_name='field_location';
  	  $locations = field_get_items('node', $record, $field_name);
  	  if (!empty($locations)){
  	    $location = field_view_value('node', $record, $field_name, $locations[0] );
  	  }

      // Filter array with country and city if provided
      $add_item = false;
      if( !isset($country) && !isset($city) ) 
      {
        $add_item = true;
      }
      else if ( isset($country) && !isset($city) && strtolower($country) == strtolower($location['#location']['country']) ) 
      {
        $add_item = true;
      }
      else if ( !isset($country) && isset($city) && strtolower($city) == strtolower($location['#location']['city']) ) 
      {
        $add_item = true;
      }
      else if ( isset($country) && isset($city) && strtolower($country) == strtolower($location['#location']['country']) && strtolower($city) == strtolower($location['#location']['city']) ) 
      {
        $add_item = true;
      }

      if ($add_item) {
        $item['id']           = $key;
        $item['name']         = $record->title;
        $item['country']      = $location['#location']['country'];
        $item['city']         = $location['#location']['city'];
        $item['venue_id']     = $record->field_provider_venue_id['und'][0]['value'];
        $item['venue']        = $location['#location']['name'];
        $item['date']         = $record->field_date['und'][0]['value'] ? (string) strtotime($record->field_date['und'][0]['value']) : null;
        $item['changed']      = $record->changed;

        $events[] = $item;
        $counter++;
      }
    }

  }

  return $events;

}


/**
 * Tonspion API helper function.
 */
function tonspion_api_get_artist_albums( $nid, $limit = null, $from_date = null ) {

  $nids = tonspion_artist_albums( $nid );
  $album_items = entity_load('node', $nids);
  $from_date = (!empty($from_date)) ? $from_date : strToTime('-1 year'); // 3 months by default
  $counter = 0;
  $albums = array();

  //Map properties
  foreach ($album_items as $key => $record) {

    // Check number of items limit
    if( !empty($limit) && $counter >= $limit ){
      break;
    } 

    // Add item if its date is bigger than $from_date
    if( $record->created >= $from_date){

  	  //Headline
  	  $field_name='field_album_headline_review';
  	  $headlines = field_get_items('node', $record, $field_name);
  	  if (!empty($headlines)){
  	    $headline = field_view_value('node', $record, $field_name, $headlines[0] );
  	  }

  	  //Image
  	  $field_name='field_album_image';
  	  $images = field_get_items('node', $record, $field_name);
  	  if (!empty($images)){
  	    $image = field_view_value('node', $record, $field_name, $images[0] );
  	  }	  

    	$item['id'] 				= $key;
    	$item['title'] 			= $record->title;
    	$item['headline'] 	= $headline['#markup'];
    	$item['created'] 		= $record->created;
    	$item['changed'] 		= $record->changed;
    	$item['image_url'] 	= image_style_url('medium', $image['#item']['uri']);
    	$item['url'] 				= drupal_get_path_alias('node/'.$key);
    	$item['type'] 			= $record->type;

    	$albums[] = $item;
      $counter++;
    }
  }

  return $albums;
}


/**
 * Tonspion API helper function.
 */
function tonspion_api_get_artist_mp3s( $nid ) {

  return array_values( tonspion_artist_mp3s( $nid , TRUE) );

}

/**
 * Tonspion API helper function.
 */
function tonspion_api_get_artist_videos( $nid, $limit = null, $from_date = null ) {

  $nids = tonspion_artist_videos( $nid );
  $videos_items = entity_load('node', $nids);
  $from_date = (!empty($from_date)) ? $from_date : strToTime('-1 year'); // 3 months by default
  $counter = 0;
  $videos = array();
 
  //Map properties
  foreach ($videos_items as $key => $record) {

    // Check number of items limit
    if( !empty($limit) && $counter >= $limit ){
      break;
    } 

    // Add item if its date is bigger than $from_date
    if( $record->created >= $from_date)
    {
        //Image
        $field_name='field_video_image';
        $images = field_get_items('node', $record, $field_name);
        if (!empty($images)){
          $image = field_view_value('node', $record, $field_name, $images[0] );
        }   

        $item['id']         = $key;
        $item['title']      = $record->title;
        $item['created']    = $record->created;
        $item['changed']    = $record->changed;
        $item['image_url']  = image_style_url('medium', $image['#item']['uri']);
        $item['url']        = drupal_get_path_alias('node/'.$key);

        $videos[] = $item;
        $counter++;
    }

  }

  return $videos;
}


/**
 * Tonspion API helper function.
 */
function tonspion_api_get_latest_updates( $id, $country = null, $city = null ) {

  $artist['news'] = tonspion_api_get_artist_news( $id, TONSPION_API_LIMIT, strToTime('-3 months') );
  $artist['albums'] = tonspion_api_get_artist_albums( $id, TONSPION_API_LIMIT, strToTime('-3 months') );
  //$artist['mp3s'] = tonspion_api_get_artist_mp3s( $id );
  $artist['videos'] = tonspion_api_get_artist_videos( $id, TONSPION_API_LIMIT, strToTime('-3 months') );
  $artist['events'] = tonspion_api_get_artist_events( $id, $country, $city, TONSPION_API_LIMIT, strToTime('-3 months') );
  //$artist['related'] = tonspion_api_get_artist_related( $id );

  return array( $id => $artist );
}



/**
 * Tonspion API helper function.
 */
function tonspion_api_get_city( $country = null, $city = null ) {

  // There is no function in tonspion.module to get city events as country an city are not drupal entities
  // So we build the query
  $events = array();

  //Get all events with that city and country
  $query = db_select('node','n');
  $query->condition('n.type','event');
  $query->condition('n.status','1');

  $query->join('field_data_field_location', 'fl', 'fl.entity_id = n.nid');
  $query->join('location', 'l', 'l.lid = fl.field_location_lid'); 

  $query->addField('n', 'nid');

  $query->condition('l.country', $country);
  $query->condition('l.city', $city);

  $result = $query->execute();

  $nids = array();
  foreach($result as $record){
    $nids[]= $record->nid;
  }  

  $event_items = entity_load('node', $nids);
  
  //Map properties
  foreach ($event_items as $key => $record) {

    //Location
    $field_name='field_location';
    $locations = field_get_items('node', $record, $field_name);
    if (!empty($locations)){
      $location = field_view_value('node', $record, $field_name, $locations[0] );
    }

    $item['id']           = $key;
    $item['name']         = $record->title;
    $item['country']      = $location['#location']['country'];
    $item['city']         = $location['#location']['city'];
    $item['venue_id']     = $record->field_provider_venue_id['und'][0]['value'];
    $item['venue']        = $location['#location']['name'];
    $item['date']         = $record->field_date['und'][0]['value'] ? (string) strtotime($record->field_date['und'][0]['value']) : null;
    $item['changed']      = $record->changed;

    $events[] = $item;

  }

  return $events;

}

/**
 * Tonspion API helper function.
 */
function tonspion_api_get_artist_related( $nid ) {

  $artists_nids = tonspion_related_artists( $nid );
  $artists_nodes = entity_load('node', $artists_nids);
  $artists = array();

  //Map properties
  foreach ($artists_nodes as $key => $value) {
  	$item['id'] = $key;
  	$item['title'] = $value->title;
  	$artists[] = $item;
  }

  return $artists;
}
