<?php

/**
 * @file
 * Tonspion - basic Feeds functions and hook implementations.
 */


/**
 * Implements hook_feeds_plugins().
 */
function tonspion_feeds_plugins() {

  $path = drupal_get_path('module', 'tonspion') . '/plugins';
  $info = array();

  if (extension_loaded('SimpleXML')) {
    $info['TonspionFeedsSyndicationParser'] = array(
      'name' => 'Tonspion syndication parser',
      'description' => 'Parse RSS and Atom feeds. Extended from common FeedsSyndicationParser parser.',
      'help' => 'Parse XML feeds in RSS 1, RSS 2 and Atom format, including channel data.',
      'handler' => array(
        'parent' => 'FeedsSyndicationParser',
        'class' => 'TonspionFeedsSyndicationParser',
        'file' => 'TonspionFeedsSyndicationParser.inc',
        'path' => $path,
      ),
    );
  }

  return $info;
}


/**
 * Implements hook_feeds_processor_targets_alter().
 *
 * @see FeedsNodeProcessor::getMappingTargets().
 */
function tonspion_feeds_processor_targets_alter(&$targets, $entity_type, $bundle_name) {

  foreach (field_info_instances($entity_type, $bundle_name) as $name => $instance) {

    $info = field_info_field($name);

    if ($info['type'] == 'entityreference') {
      $targets[$name] = array(
        'name'        => check_plain($instance['label'] . t(' (Entity reference contained in Entity label)')),
        'callback'    => 'tonspion_set_target_artist',
        'description' => t('The field instance @label of @id matched by Entity label.', array(
          '@label' => $instance['label'],
          '@id'    => $name,
        )),
      );
    }

  }

}


/**
 * Entity reference callback for mapping.
 *
 * When the callback is invoked, $target contains the name of the field the
 * user has decided to map to and $value contains the value of the feed item
 * element the user has picked as a source.
 *
 * @param $source
 *   A FeedsSource object.
 * @param $entity
 *   The entity to map to.
 * @param $target
 *   The target key on $entity to map to.
 * @param $value
 *   The value to map. MUST be an array.
 *
 * @see $value entityreference_set_target_artist
 */
function tonspion_set_target_artist($source, $entity, $target, $value) {

  // Don't do anything if we weren't given any data.
  if (empty($value)) {
    return;
  }

  // Assume that the passed in value could really be any number of values.
  if (is_array($value)) {
    $values = $value;
  }
  else {
    $values = array($value);
  }

  // Determine the field we are matching against.
  list($target, $match_key) = explode(':', $target, 2);


  // Get some useful field information.
  $info = field_info_field($target);

  // Set the language of the field depending on the mapping.
  $language = isset($mapping['language']) ? $mapping['language'] : LANGUAGE_NONE;

  // Iterate over all values.
  $iterator = 0;
  $field = isset($entity->$target) ? $entity->$target : array();

  foreach ($values as $value) {

    // Only process if this value was set for this instance.
    if ($value) {
        $handler = entityreference_get_selection_handler($info);
        $node_types = $handler->field['settings']['handler_settings']['target_bundles'];
        
        //Selects first node whose title is contained in value (should be an artist name)
        $entity_id = db_select('node', 'n')
            ->fields('n', array('nid'))
            ->condition( 'type', $node_types, 'IN')
            ->where("LOWER(:value) LIKE CONCAT('%', n.title, '%')", array(':value' => $value))
            ->execute()
            ->fetchField();

      /*
       * Only add a reference to an existing entity ID if there exists a
       * mapping between it and the provided GUID.  In cases where no such
       * mapping exists (yet), don't do anything here.  There may be a mapping
       * defined later in the CSV file.  If so, and the user re-runs the import
       * (as a second pass), we can add this reference then.  (The "Update
       * existing nodes" option must be selected during the second pass.)
       */

      if ($entity_id) {
        // Assign the target ID.
        $field[$language][$iterator]['target_id']   = $entity_id;
      }
      else /* there is no $entity_id, no mapping */ {

        /*
         * Feeds stores a hash of every line imported from CSVs in order to
         * make the import process more efficient by ignoring lines it's
         * already seen.  We need to short-circuit this process in this case
         * because users may want to re-import the same line as an update later
         * when (and if) a map to a reference exists.  So in order to provide
         * this opportunity later, we need to destroy the hash.
         */
        unset($entity->feeds_item->hash);
        $source->log('entityreference', t('No existing entity found for entity @source_id entityreference to source entity @value', array('@source_id' => $entity->feeds_item->entity_id, '@value' => $value)));
      }

    }

    // Break out of the loop if this field is single-valued.
    if ($info['cardinality'] == 1) {
      break;
    }
    $iterator++;

  }

  // Add the field to the entity definition.
  $entity->{$target} = $field;
}


/**
 * Invoked before a feed item is updated/created/replaced.
 *
 * This is called every time a feed item is processed no matter if the item gets
 * updated or not.
 *
 * @param FeedsSource $source
 *  The source for the current feed.
 * @param array $item
 *  All the current item from the feed.
 * @param int|null $entity_id
 *  The id of the current item which is going to be updated. If this is a new
 *  item, then NULL is passed.
 *
 * @see hook_feeds_presave()
 */
function tonspion_feeds_presave(FeedsSource $source, $entity, $item) {

  // Only consider nodes that reference to existing artists
  $entity->feeds_item->skip = empty($entity->field_ref_artist);

}




