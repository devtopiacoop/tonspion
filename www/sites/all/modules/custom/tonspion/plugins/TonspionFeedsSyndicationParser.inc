<?php

/**
 * @file
 * Overrides FeedsSyndicationParser methods
 */

/**
 * Class definition for Common Syndication Parser.
 *
 * Parses RSS and Atom feeds.
 */
class TonspionFeedsSyndicationParser extends FeedsSyndicationParser {

  /**
   * Implements FeedsParser::parse().
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {

    //We use tonspion parser instead of common
    //In sites/all/libraries or sites/all/modules/contrib/feeds/libraries
    //feeds_include_library('tonspion_syndication_parser.inc', 'tonspion_syndication_parser');
    require drupal_get_path('module', 'tonspion') . '/libraries/tonspion_syndication_parser.inc';

    $feed = tonspion_syndication_parser_parse($fetcher_result->getRaw());

    $result = new FeedsParserResult();
    $result->title = $feed['title'];
    $result->description = $feed['description'];
    $result->link = $feed['link'];

    //We add other channel data
    //$result->docs = $feed['docs'];
    $result->update = $feed['updated'];
    $result->language = $feed['language'];

    if (is_array($feed['items'])) {
      foreach ($feed['items'] as $item) {
        if (isset($item['geolocations'])) {
          foreach ($item['geolocations'] as $k => $v) {
            $item['geolocations'][$k] = new FeedsGeoTermElement($v);
          }
        }
        $result->items[] = $item;
      }
    }
    return $result;
  }

  /**
   * Return mapping sources.
   *
   * At a future point, we could expose data type information here,
   * storage systems like Data module could use this information to store
   * parsed data automatically in fields with a correct field type.
   */
  public function getMappingSources() {

    //dsm('TonspionFeedsSyndicationParser::getMappingSources');

    return array(
      'channel:title' => array(
        'name' => t('Channel:title'),
        'description' => t('Title of the channel.'),
      ),
      'channel:description' => array(
        'name' => t('Channel:description'),
        'description' => t('Description of the channel.'),
      ),
      'channel:link' => array(
        'name' => t('Channel:link'),
        'description' => t('Link of the channel.'),
      ),
      // 'channel:docs' => array(
      //   'name' => t('Channel:docs'),
      //   'description' => t('Channel docs link.'),
      // ),
      'channel:updated' => array(
        'name' => t('Channel:updated'),
        'description' => t('Channel last update date.'),
      ),
      'channel:language' => array(
        'name' => t('Channel:language'),
        'description' => t('Channel language (ex: en)'),
      ),
    ) + parent::getMappingSources();
  }
}
