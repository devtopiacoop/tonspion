<?php

/**
 * @file
 * Contains the artist node from URL argument default plugin.
 * It may also look for field_ref_artist if node is not of type artist
 */

/**
 * Default argument plugin to extract a node via menu_get_object
 *
 * This plugin actually has no options so it odes not need to do a great deal.
 */
class tonspion_plugin_argument_default_artist extends views_plugin_argument_default {

  function get_argument() {

    foreach (range(1, 3) as $i) {
      $node = menu_get_object('node', $i);
      if (!empty($node) && $node->type=='artist') {
        return $node->nid;
      }else if ($node->type!='artist' && !empty($node->field_ref_artist)){
        return $node->field_ref_artist['und'][0]['target_id'];
      }
    }

    if (arg(0) == 'node' && is_numeric(arg(1))) {
      $node = node_load(arg(1));
      if (!empty($node) && $node->type=='artist') {
        return $node->nid;
      }else if ($node->type!='artist' && !empty($node->field_ref_artist)){
        return $node->field_ref_artist['und'][0]['target_id'];
      }
    }

  }

}
