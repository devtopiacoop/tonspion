<?php 

/**
 * Implements hook_ds_fields_info_alter().
 */
function tonspion_ds_fields_info_alter(&$fields, $entity_type) {

  if (isset($fields['title'])) {
	  $fields['title']['function'] = 'tonspion_render_field';
	  $fields['title']['properties']['settings']['artist'] = array('type' => 'select', 'options' => array('no', 'yes'), 'description' => t('Include artist name before title. Artist field should be enabled.'));
	  $fields['title']['properties']['settings']['artist separator'] = array('type' => 'textfield', 'description' => t('Separator between Artist and Title'));
	  $fields['title']['properties']['default']['artist'] = 0;
	  $fields['title']['properties']['default']['artist separator'] = ' ';
	}

}



