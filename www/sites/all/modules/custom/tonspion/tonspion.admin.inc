<?php




/**
 * Tonspion 7Digital settings.
 */
function tonspion_gpt_settings() {

  $form = array();

  $default_code = "
googletag.cmd.push(function() {

	//Adslot 1 declaration
	gptadslots[1]= googletag.defineSlot('/16916245/tonspion.de', [[300,250]],'div-gpt-ad-206679526009306368-1').addService(googletag.pubads());

	//Adslot 2 declaration
	gptadslots[2]= googletag.defineSlot('/16916245/tonspion.de', [[300,600]],'div-gpt-ad-206679526009306368-2').addService(googletag.pubads());

	//Adslot 3 declaration
	gptadslots[3]= googletag.defineSlot('/16916245/tonspion.de', [[970,250],[728,90]],'div-gpt-ad-206679526009306368-3').addService(googletag.pubads());

	//Adslot 4 declaration
	gptadslots[4]= googletag.defineSlot('/16916245/tonspion.de', [[1,1]],'div-gpt-ad-206679526009306368-4').addService(googletag.pubads());

	//Adslot 5 declaration
	gptadslots[5]= googletag.defineSlot('/16916245/tonspion.de', [[440,1800]],'div-gpt-ad-206679526009306368-5').addService(googletag.pubads());

	//Adslot 6 declaration
	gptadslots[6]= googletag.defineSlot('/16916245/tonspion.de', [[440,1801]],'div-gpt-ad-206679526009306368-6').addService(googletag.pubads());

	googletag.pubads().enableSingleRequest();
	googletag.pubads().enableAsyncRendering();
	googletag.pubads().collapseEmptyDivs(true);
	googletag.enableServices();
});";

  $form['#prefix']  = '<h2>'.t('Google Publish Tags').'</h2>';

  $form['tonspion_gpt_code'] = array(
    '#type' => 'textarea',
    '#title' => t('GPT code'),
    '#description' => t('Please provide HEAD code from by Advice (http://viceland.de/dev/advice.html).'),
    '#default_value' => variable_get('tonspion_gpt_code', $default_code),
    '#rows' => 20
  );


  return system_settings_form($form);
}