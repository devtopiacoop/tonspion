<?php

/**
 *  Render callback from hook menu
 */
function tonspion_predownload_mp3_render( $mp3_nid , $album_nid ) {
	$mp3_node = node_load($mp3_nid);
	$album_node = node_load($album_nid);
	$artist_node = node_load($album_node->field_ref_artist['und'][0]['target_id']);
	return theme('tonspion_mp3_predownload_page',array( 'mp3_node'=>$mp3_node , 'album_node'=>$album_node , 'artist_node'=>$artist_node ));
}

/**
 *  Theme for pre download page
 */
function theme_tonspion_mp3_predownload_page($variables) {
	
	drupal_set_title(t('Free Download & Stream') . ' - ' . $variables['mp3_node']->title);

	$output = '
	<h4 class="title">' . t('Free Download & Stream') . '</h4>
	<div class="mp3-predownload-wrapper">

		<div class="mp3-predownload-data"> 

	        <div class="mp3-predownload-data-artist"><span>' . t('Artist') . ':</span> <strong>' . $variables['artist_node']->title . '</strong></div>    
	        <div class="mp3-predownload-data-title"><span>' . t('Title') . ':</span> <strong>' . $variables['mp3_node']->title . '</strong></div>    
	        <div class="mp3-predownload-data-album"><span>' . t('Album') . ':</span> <strong>' . $variables['album_node']->title . '</strong></div>    

	    </div>

	    <script type="text/javascript">
			<!--
			    jQuery(document).ready(function()
			    {
			    	setTimeout(\'jQuery("#go-to-download-submit").click()\', 6000);
			    });
			//-->
		</script>
		
		<div class="mp3-predownload-loader">
		    <img src="'. base_path() . drupal_get_path('module','tonspion') . '/loader.gif" alt="">
		</div>

		<div class="mp3-predownload-tooltip">
			' . ( !empty($variables['mp3_node']->field_mp3_download_tip) ? $variables['mp3_node']->field_mp3_download_tip['und'][0]['value']:t("Wir leiten dich jetzt weiter auf eine externe Webseite, wo du den Song hören oder runterladen kannst") ) . '
		</div>

		<form id="mp3-predownload-form" method="post" accept-charset="UTF-8" name="mp3_predownload_form" action="' . $variables['mp3_node']->field_mp3_downloadlink['und'][0]['url'] . '">
		    <div>
		      <fieldset>
		        <div class="field buttons">       
		            <input type="submit" class="form-submit" value="' . t('Weiter') . '" id="go-to-download-submit" name="goto_link">
		        </div>        
		      </fieldset>
		    </div>
		</form>
	</div>
	';


	return $output;
}


?>