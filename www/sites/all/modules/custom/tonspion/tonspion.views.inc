<?php

/**
 * Implements hook_views_plugins().
 */
function tonspion_views_plugins() {
  return array(
    'argument default' => array(
      'artist' => array(
        'title' => t('Artist ID from URL or field_ref_artist'),
        'handler' => 'tonspion_plugin_argument_default_artist',
        'path' => drupal_get_path('module', 'tonspion') . '/plugins',
       ),
    ),
  );
}


/**
 * Implements hook_views_plugins().
 */
function tonspion_views_query_alter(&$view, &$query){
  
	// Alter query to include in view all nodes related with one in the argument
	if ($view->name == 'artists' && $view->current_display == 'block_artists_related'){
  	
    //Related artists
  	$field = trim(strstr($query->where[0]['conditions'][0]['field'], ':'));

  	$related_artists = tonspion_related_artists( $query->where[0]['conditions'][0]['value'][$field] );

		// If there are relates artist overrides nids to select, if not selects non-existing node (nid=0)
		$related_artists = empty($related_artists) ? array(0) : $related_artists;

		//Query override
  	$query->where[0]['conditions'][0]['field'] = 'node.nid';
  	$query->where[0]['conditions'][0]['value'] = $related_artists;
  	$query->where[0]['conditions'][0]['operator'] = 'in';

	} else if ($view->name == 'album_reviews' && $view->current_display == 'block_albums_genre') {

    //Related artists
    $field = trim(strstr($query->where[0]['conditions'][0]['field'], ':'));

    $artists_albums = tonspion_artist_albums( $query->where[0]['conditions'][0]['value'][$field] );

    // Overrides query in order to include all artist albums instead of artist
    if(!empty($artists_albums)){
      
      $query->where[0]['conditions'][0]['field'] = 'node.nid';
      $query->where[0]['conditions'][0]['value'][$field] = $artists_albums;
      $query->where[0]['conditions'][0]['operator'] = 'in';

    }

  }

}