<?php

/**
 * @file
 * Tonspion - basic CTools functions and hook implementations.
 */


/**
 * Implements hook_ctools_plugin_api().
 */
function tonspion_ctools_plugin_api($owner, $api) {
  if ($owner == 'feeds' && $api == 'plugins') {
    return array('version' => 1);
  }
}

/**
 * Implements hook_ctools_plugin_type().
 */
function tonspion_ctools_plugin_type() {
  return array(
    'plugins' => array(
      'cache' => TRUE,
      'use hooks' => TRUE,
      'classes' => array('handler'),
    ),
  );
}

