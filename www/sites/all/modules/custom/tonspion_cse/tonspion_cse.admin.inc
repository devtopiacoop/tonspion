<?php




/**
 * Tonspion 7Digital settings.
 */
function tonspion_cse_settings() {

  $form = array();

  $form['#prefix']  = '<h2>'.t('Google CSE settings').'</h2>';

  $form['tonspion_cse_engine_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Google CSE engine ID'),
    '#description' => t('Please input the engine id from Google CSE.'),
    '#default_value' => variable_get('tonspion_cse_engine_id', 'partner-pub-6944161184803113:1183533715'),
  );

  return system_settings_form($form);
}