<?php



/**
 *  Theme for pre download page
 */
function theme_tonspion_cse_page() {
	
	$output = "
<script>
  (function() {
    var cx = '".variable_get('tonspion_cse_engine_id', 'partner-pub-6944161184803113:1183533715')."';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:searchresults-only></gcse:searchresults-only>
";

	return $output;
}


?>