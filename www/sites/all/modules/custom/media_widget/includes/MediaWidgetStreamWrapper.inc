<?php

/**
 *  @file media_widget/includes/MediaWidgetStreamWrapper.inc
 *
 *  Create a Widget object class for the Media/Resource module.
 */

/**
 *  Create an instance like this:
 *  $youtube = new MediaYouTubeStreamWrapper('youtube://v/[video-code]');
 */
class MediaWidgetStreamWrapper extends MediaReadOnlyStreamWrapper {


  static function getMimeType($uri, $mapping = NULL) {
    return 'text/html';
  }

  function getTarget($f) {
    return FALSE;
  }

}
