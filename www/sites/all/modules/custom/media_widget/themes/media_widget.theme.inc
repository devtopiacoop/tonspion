<?php

/**
 * @file media_youtube/themes/media_youtube.theme.inc
 *
 * Theme and preprocess functions for Media: YouTube.
 */

/**
 * Preprocess function for theme('media_widget_object').
 */
function media_widget_preprocess_media_widget_object(&$variables) {
  $variables['classes'] = 'widget object';
}
