<?php

/**
 * @file media_widget/themes/media-widget.tpl.php
 *
 * Template file for theme('media_widget_object').
 *
 *
 */

?>
<div class="<?php print $classes; ?> media-widget-<?php print $id; ?>">
  <?php print $content;?>
</div>
