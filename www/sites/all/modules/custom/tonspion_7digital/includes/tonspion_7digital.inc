<?php

/**
 * @file
 * Tonspion 7Digital - 7digital integration
 * @author jorge.maestre@devtopia.coop
 * @author leandro.vazquez@devtopia.coop
 */

use SevenDigital\ApiClient;


/**
 * Helper funtion that returns a 7Digital API client
 *
 * @return ApiClient
 * @author
 **/
function tonspion_7digital_client()
{
  // Register the autoloader and ensure the library is available.
  composer_manager_register_autoloader();

  if (!class_exists('SevenDigital\ApiClient')) {
    $message = t('The SevenDigital\ApiClient class was not found. Make sure the package is installed via Composer. More info: https://github.com/cpliakas/composer-manager-docs/blob/master/README.md#installation');
    throw new \RuntimeException($message);
  }

  return new ApiClient(variable_get('tonspion_7digital__consumer_key', TONSPION_7DIGITAL_DEFAULT_CONSUMER));

}


/**
 * Get yesterday album releases from 7Digital
 *
 * @return array
 *         Array of 7Digital XML objects
 * @author
 **/
function tonspion_7digital_new_releases()
{
  /************
   ** PERIOD **
   * **********/
  $period = variable_get('tonspion_7digital_releases_last_check', date("Ymd", strtotime('-7 days', time()) ) );

  $last_check = date("Ymd", strtotime($period,  time()) );
  $month = date("Ymd", strtotime('-1 month',  time()) );
  $yesterday  = date("Ymd", strtotime('-1 day', time()) );

  //Continue only if last check was before yesterday
  if ($yesterday <= $last_check) {
	  _my_drush_log( "last releases check after yesterday. not fetching.", 'warning' );
    watchdog('tonspion_7digital',
      'Last releases check run after yesterday, so not fetching till tomorrow.',
      array(),
      WATCHDOG_WARNING);
	  return null;
  }

  //API Client
  $client = tonspion_7digital_client();

  //@TODO: catch up (better) errors with Composer
  if (!$client) {
	  _my_drush_log( "Unable to initilize the 7Digital service. Possible problem with Composer :(", 'error' );
	  watchdog('tonspion_7digital', 
      'Unable to initilize the 7Digital service<br/> Possible problem with Composer :(',
      array(),
      WATCHDOG_WARNING);
  }

  //API Service
  $service = $client->getReleaseService();

  //Request options
  $options = array(
      'country'           => TONSPION_7DIGITAL_LANGUAGE,
      'pageSize'          => TONSPION_7DIGITAL_PAGE_SIZE,
      'fromDate'          => max($last_check, $month), // API only allows 31 days of period
      'toDate'            => $now,
      'imageSize'         => 350
  );

  //Service call
  try {

    $response = $service->bydate( $options );
    variable_set('tonspion_7digital_releases_last_check', $yesterday);

    watchdog('tonspion_7digital', 
      'New releases checked from %from to %to.', 
      array('%from'=>$options['fromDate'], '%to'=>$now), 
      WATCHDOG_INFO);

    return $response;

  } catch (Exception $e) {

    _my_drush_log( "Exception with release/bydate service ". $e, 'error' );
    watchdog('tonspion_7digital', 
      'New releases: exception with release/bydate service:<br/> %e', 
      array('%e'=>$e), 
      WATCHDOG_WARNING);

  }

  return NULL;

}


/**
 * Get album releases from 7Digital to refresh in Drupal
 *
 * @return array
 *         Array of 7Digital XML objects
 *
 *
 **/
function tonspion_7digital_new_albums_update()
{

  /************
   ** PERIOD **
   * **********/
  $period = variable_get('tonspion_7digital_albums_refresh', date('-1 year',  $periodtimestamp) );
  $periodtimestamp = strtotime($period, time());
  /************
   ** PERIOD **
   * **********/

  $last_check = date("Ymd", $periodtimestamp);
  $a_year_ago = date("Ymd", strtotime('-1 day', time())); //CAUTION: step each API call is setted to -1 YEAR
  $month = date("Ymd", strtotime('-1 month',  time()) );

// Continue only if last check was before yesterday
  if ($a_year_ago <= $last_check) {
	  _my_drush_log( "last albums check after yesterday. not fetching.", 'warning' );
    watchdog('tonspion_7digital',
      'Albums update check run after yesterday, so not fetching till tomorrow.',
      array(),
      WATCHDOG_WARNING);    
	  return null;
 }

  //API Client
  $client = tonspion_7digital_client();

  //@TODO: catch up (better) errors with Composer
  if (!$client) watchdog('tonspion_7digital', 
    'Unable to initilize the 7Digital service<br/> Possible problem with Composer :(', 
    array(),
    WATCHDOG_WARNING);

  //API Service
  $service = $client->getReleaseService();

  //Request options
  $options = array(
      'country'           => TONSPION_7DIGITAL_LANGUAGE,
      'pageSize'          => TONSPION_7DIGITAL_PAGE_SIZE,
      'fromDate'          => max($last_check, $month), // API only allows 31 days of period
      'toDate'            => $now,
      'imageSize'         => 350
  );

  //Service call
  try {

    $response = $service->bydate( $options );
    variable_set('tonspion_7digital_albums_refresh', $a_year_ago);

    watchdog('tonspion_7digital', 'Albums updates checked from %from to %to.', 
      array('%from'=>$options['fromDate'], '%to'=>$now), 
      WATCHDOG_INFO);

    return $response;

  } catch (Exception $e) {

  	_my_drush_log( "Exception with release/bydate service ". $e, 'error' );
    watchdog('tonspion_7digital', 
      'Albums update: exception with release/bydate service:<br/> %e', 
      array('%e'=>$e), 
      WATCHDOG_WARNING);

  }

  return NULL;

}


/**
 * Get existing artists info  in Drupal to fetch image (if inexistent) from 7Digital
 *
 * @return array
 *         Array of Artist nodes
 *
 **/
function tonspion_7digital_existing_artists_update()
{
      //artists in Drupal without image, never fetched from 7Digital. Unfortunately, EFQ hasn't isNull()

//Get all the entities that DO have values
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'artist')
        ->fieldCondition('field_artist_image', 'fid', 'NULL', '!=');
  $result = $query->execute();

  if (is_array(@$result['node'])) {
    //Now get all the other entities, that aren't in the list you just retrieved
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
          ->entityCondition('bundle', 'artist')
          ->entityCondition('entity_id', array_keys($result['node']), 'NOT IN');
    $artists_without_image = $query->execute();
  }


  foreach ($artists_without_image['node'] as $artist) {
  	 $nodeartist = node_load($artist->nid);
       if (!$nodeartist->field_artist_image['und'][0]['fid']){
  		 $_[] = $nodeartist;
  	 }
  }

  return $_;
}


/**
 * Get album nodes matching a 7D release XML
 *
 * @return array node
 * @author
 **/
function tonspion_7digital_get_albums_matching_7D( $xml_release )
{
  $albums = array();

  if (is_numeric($xml_release->{"@attributes"}->id)) {
    $xml_release->id = $xml_release->{"@attributes"}->id;
  }else{
    _my_drush_log( "ERROR: no ID found for release ". $xml_release->title, 'error' );
  }


  $query = new EntityFieldQuery;

  // Check if release ID is stored in any album
  $result = $query
  ->entityCondition('entity_type', 'node')
  ->entityCondition('bundle', 'album')
  ->fieldCondition('field_album_sevendigital_id', 'value', $xml_release->id, '=')
  ->execute();

  if (isset($result['node'])){
  // If there is any album with that 7D release Id returns then

	//DEBUG:
	_my_drush_log( $xml_release->id . " id album release exists", 'notice' );

    $nids = array_keys($result['node']);
    $albums = entity_load('node', $nids);

  } else {
  // If there is no album with that 7D release id

    // Look for an album matching title
    $matching_albums = $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'album')
    ->propertyCondition('title', $xml_release->title)
    ->execute();

    // If there is an album matching title
    if (isset($matching_albums['node'])){

      $albums_nids = array_keys($result['node']);
      $matching_albums = entity_load('node', $albums_nids);

      // Look also for an artist matching 7d Id
      if (isset($xml_release->artist['id'])) {

        $artists_matching = $query
        ->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'artist')
        ->fieldCondition('field_artist_sevendigital_id', 'value', $xml_release->artist['id'], '=')
        ->execute();

        // If there is an album with same title and artist 7D id
        if (isset($artists_matching['node'])){
          $artists_nids = array_keys($artists_matching['node']);
          foreach ($matching_albums as $album) {

            //Get matching title albums that also match Artist 7D id
			//FIXME: NEVER GOT INTO HERE (?)

            if( in_array($album->field_artist_sevendigital_id, $artists_nids)){
              $albums[] = $album;
			  print_r($albums);die("a");
              // @TODO: update album 7D data and return it


            }
          }
        }

      }

    }

  }

  return $albums;

}

/**
 * Get artist from a 7D artist XML
 *
 * @return array artist nid
 * @author
 **/
function tonspion_7digital_get_artists_matching_7D( $xml_artist )
{

  $query = new EntityFieldQuery;

  // Look also for an artist matching 7d Id
  if (isset( $xml_artist->{"@attributes"}->id )) {

  //DEBUG:
  //echo "looking for " . $xml_artist->{"@attributes"}->id . " artist id\n\r";

    // Look for ID
    $matching_artists = $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'artist')
    ->fieldCondition('field_artist_sevendigital_id', 'value', $xml_artist->{"@attributes"}->id, '=')
    ->execute();

    if (isset($matching_artists['node'])){
    // If there is an artist matching 7D id

      return array_keys($matching_artists['node']);

    } else {
    // No matchig by ID, try by artist name

      $matching_artists = $query
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'artist')
      ->propertyCondition('title', $xml_artist->name, '=')
      ->execute();

      if (isset($matching_artists['node'])) {
        return array_keys($matching_artists['node']);
      }

    }

  }

  return NULL;

}

/**
 * Add artist node from a 7D artist id
 *
 * @return array artist node
 * @author
 **/
function tonspion_7digital_add_artist_from_7D( $artist_xml )
{

  if (!@$artist_xml->url && !@$artist_xml->sortName) {
	   $artist_xml = _artist_info_request_from_7D( $artist_xml );
  }

  //print_r($artist_xml); die();

  //create new artist from 7D
  $nid = _create_artist_from_7D( $artist_xml );

  _my_drush_log( "artist (nid " . $nid . ") successfully created", 'ok' );  

  return $nid;

}

/**
 * Add album node from a 7D release
 *
 * @param $release_xml 7Digital release XML object
 * @param $artist_nid artist node id
 *
 * @return array album node added
 * @author
 **/
function tonspion_7digital_add_album_from_7D( $release_xml, $artist_nid )
{

  //Add artist node from $release_xml data
  $new_album = _create_album_from_7D( $release_xml, (int) $artist_nid );

  return $new_album;
}

/**
 * Update album node (artist or genre) from a 7D release
 *
 * @param $album album node to update
 * @param $xml_release release data XML object from 7D
 *
 * @return array album node updated
 * @author
 **/
function tonspion_7digital_update_album_from_7D( &$albums, $xml_release, $save_time = null )
{

	//$albums may comes as array, but should be only one Object
	if (is_array ( $albums ) ) {
		foreach ($albums as $a ) {
			 $album = $a;
		}
	}else{
		$album = $albums;
	}

	$artist_7did = $xml_release->artist->{"@attributes"}->id;

	// Look for artist in drupal db if not present in album
	if (is_numeric( $artist_7did ) && ( !is_numeric($album->field_ref_artist['und'][0]['target_id']) ) ) 
  {

		$query = new EntityFieldQuery;

        $artist_matching = $query
        ->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'artist')
        ->fieldCondition('field_artist_sevendigital_id', 'value', $artist_7did, '=')
        ->execute();

        // artist is in drupal db but not in this album: insert artist nid

        if (isset($artist_matching['node'])){

          $artist_nid = array_keys($artist_matching['node']);
		  $artist_id_in_drupal = $artist_nid[0];


	      $album->field_ref_artist['und'][0]['target_id'] = $artist_id_in_drupal;
	      node_save($album);
	      _my_drush_log( "album without artist (existent) updated. album: " . $album->title, 'ok' );


	    }else{

  		//artist is new: create in Drupal from 7D
  			$artist_id_in_drupal = _create_artist_from_7D( $artist_xml );
  			_my_drush_log( "found new artist (not existent in db) id in db: " . $artist_id_in_drupal, 'notice' );

  			$album->field_ref_artist['und'][0]['target_id'] = $artist_id_in_drupal;

  	    _my_drush_log( "album without artist (existent) updated. album: " . $album->title, 'notice' );
  		}


  		//setting processed date if needed
  		if ( $save_time ) {
  			$album->field_album_sd_update_time['und'][0]['value'] = time();
  		}
  		node_save($album);
  		die("oo-1");
  }


  // Album genres
	if ( empty($album->field_genre) ) 
  {

		//******** ACTUALIZE TAGS FROM 7D

		$release_tags = _get_release_tags_from_7D( $xml_release );

        if( $release_tags->tags->totalItems[0] > 0) {

			//prepare tags
			foreach($release_tags->xpath("//tag") as $tag){

			  if ($tag->text = "hip hop/rap") $tag->text = "hiphop";
			  if ($tag->text = "electronica") $tag->text = "electro";
			  if ($tag->text = "classical")   $tag->text = "klassik";

			  $tagobj = taxonomy_get_term_by_name($tag->text, "genres");
				foreach ( $tagobj as $tag ) {
				  $genres[] = $tag->tid;
				}
			}

			//DEBUG:
			if (empty($genres)) _my_drush_log( "tag present, but not in Drupal", 'notice' );

			for ( $i = 0; $i < count($genres); $i++ ) {
			  $album->field_genre['und'][$i]['tid'] =  $genres[$i];
			}

			if ( $save_time ) $album->field_album_sd_update_time['und'][0]['value'] = time();

			node_save($album);
			_my_drush_log(  "tags saved for " . $xml_release->title, "ok" );
			watchdog('tonspion_7Digital', 
        'Genres saved from 7D for release (%nid): %release', 
        array( '%nid'=>$album->nid, '%release' => $xml_release->title ),
        WATCHDOG_INFO);

		}else{
			//DEBUG
			_my_drush_log( "no tags for " . $xml_release->title, 'notice');
			watchdog('tonspion_7Digital', 
        'No genres found for release (%nid): %release', 
        array( '%nid'=>$album->nid, '%release' => $xml_release->title),
        WATCHDOG_INFO);
		}

		//******** END ACTUALIZE TAGS FROM 7D
	}

}


function _create_album_from_7D( $release_xml, $artist_nid ) {
  //DEBUG
  //print_r($release_xml);die();

  $response_tags = _get_release_tags_from_7D( $release_xml );
  $genres = array();

  //get label taxonomy id from xml name
  $arr_label_taxoid     = taxonomy_get_term_by_name($release_xml->label->name, "label");
  foreach ($arr_label_taxoid as $label) {
	 $label_taxoid[] = $label->tid;
  }

  //get type taxonomy id from xml type
  $arr_typealbum_taxoid = taxonomy_get_term_by_name($release_xml->type, "album_type");
  foreach ($arr_typealbum_taxoid as $taxo) {
	 $typealbum_taxoid[] = $taxo->tid;
  }

  //prepare tags, CAUTION: maybe empty from 7D :/
  if (!empty($response_tags)) {
	  foreach ( @$response_tags as $arr_tag ) {
		 $tagobj = taxonomy_get_term_by_name($arr_tag->tag->text, "genres");
		 foreach ( $tagobj as $tag ) {
			$genres[] = $tag->tid;
		 }
	  }
  }


  /**********************************
   **********************************
   ****** ALBUM NODE SAVING  ********
   **********************************
   */
  $newNode = (object) NULL;
  $newNode->title    = $release_xml->title;
  $newNode->type     = 'album';
  $newNode->created  = strtotime("now");
  $newNode->changed  = strtotime("now");
  $newNode->status   = 0;
  $newNode->comment  = 0;
  $newNode->promote  = 0;
  $newNode->moderate = 0;
  $newNode->sticky   = 0;
  $newNode->language = 'de';

  $newNode->field_ref_artist['und'][0]['target_id'] = $artist_nid;

  $genres = array_unique($genres);
  for ( $i = 0; $i < count($genres); $i++ ) {
    $newNode->field_genre['und'][$i]['tid'] =  $genres[$i];
  }

  for ( $i = 0; $i < count($typealbum_taxoid); $i++ ) {
    $newNode->field_album_type['und'][$i]['tid'] = $typealbum_taxoid[$i];
  }

  for ( $i = 0; $i < count($label_taxoid); $i++ ) {
    $newNode->field_album_label['und'][$i]['tid'] = $label_taxoid[$i];
  }

  $newNode->field_without_mp3['und'][0]['value'] = (is_object($release_xml->formats))? 1 : 0; //FIXME

  $newNode->field_is_7digital_import['und'][0]['value'] = 1;

  $newNode->field_album_sd_update_time['und'][0]['value']       = time();
  $newNode->field_album_sd_update_time['und'][0]['timezone']    = "Europe/Berlin";
  $newNode->field_album_sd_update_time['und'][0]['timezone_db'] = "UTC";
  $newNode->field_album_sd_update_time['und'][0]['date_type']   = "datestamp";


  $newNode->field_album_sevendigital_id['und'][0]['value']      = $release_xml->{"@attributes"}->id;
  //$newNode->field_album_mp3['und'][0]['target_id'] = ; //@TODO

  /****************
   * ALBUM IMAGE  *
   * **************
   */

  $url = $release_xml->image;
  $file_info = system_retrieve_file($url, 'public://albums/', TRUE);
  if($file_info->fid){
	   $newNode->field_album_image['und']['0']['fid'] = $file_info->fid;
  }

  //END ALBUM IMAGE


  node_object_prepare($newNode);
  $newNode->uid      = 573; //settings

  //DEBUG
  //print_r($newNode);die();

  try {

    node_save($newNode);

    if ($newNode->nid){
      watchdog('tonspion_7digital',
        'New album added (%nid): %name',
        array( '%nid' => $newNode->nid, '%name' => $newNode->title ), 
        WATCHDOG_INFO);     
    }
    else
    {
      watchdog('tonspion_7digital',
        'Could not add album: %name',
        array('%name' => $xml_release->title), 
        WATCHDOG_WARNING); 

    }

  }catch (Exception $e) {
    watchdog('tonspion_7digital', $e, array(), WATCHDOG_ERROR);
  }

  //DEBUG
  //_my_drush_log( $release_xml->title. " album created successfully", 'ok' );

  return(!empty($newNode->nid))? $newNode : FALSE;
}


function _create_artist_from_7D ($artist_xml) {

  if ( !@$artist_xml->image &&  !@$artist_xml->sortName ) {
	   $artist_xml = _artist_info_request_from_7D( $artist_xml );
  }

  if (!$artist_xml->image) {
	  print_r($artist_xml);
	  die();
  }

  /***********************************
   ***********************************
   ****** ARTIST NODE SAVING  ********
   ***********************************
   */
  $newNode = new stdClass();
  $newNode->title    = $artist_xml->name;
  $newNode->type     = 'artist';
  $newNode->created  = strtotime("now");
  $newNode->changed  = strtotime("now");
  $newNode->status   = 0;
  $newNode->comment  = 0;
  $newNode->promote  = 0;
  $newNode->moderate = 0;
  $newNode->sticky   = 0;
  $newNode->language = 'de';

  $newNode->field_artist_sprache['und'][0]['value'] = 'de';

  if ($artist_xml->appearsAs) $artist_xml->sortName = $artist_xml->appearsAs;
  $newNode->field_second_bandname['und'][0]['value'] = $artist_xml->sortName;
  $newNode->field_artist_sprache['und'][0]['tid'] = 7; //FIXME (?!?!?!?!)

  $newNode->field_artist_sevendigital_id['und'][0]['value'] = $artist_xml->{"@attributes"}->id;

  $newNode->field_artist_sdid_update_time['und'][0]['value']       = time();
  $newNode->field_artist_sdid_update_time['und'][0]['timezone']    = "Europe/Berlin";
  $newNode->field_artist_sdid_update_time['und'][0]['timezone_db'] = "UTC";
  $newNode->field_artist_sdid_update_time['und'][0]['date_type']   = "datestamp";

  $newNode->field_artist_weblink['und'][0]['url']          = $artist_xml->url; //???
  $newNode->field_artist_sevendigital_url['und'][0]['url'] = $artist_xml->url;

  node_object_prepare($newNode);
  $newNode->uid      = 573; //settings

  /****************
   * ARTIST IMAGE *
   * **************
   */
  if ($artist_xml->image) {
	  //preparing 200px image form xml
	  //$artist_xml->image = preg_replace("(_150)", "_200", $artist_xml->image, -1);

	  $url = $artist_xml->image;
	  $file_info = system_retrieve_file($url, 'public://', TRUE);
	  if($file_info->fid){
		$newNode->field_artist_image['und']['0']['fid'] = $file_info->fid;
	  }
  }else _my_drush_log( "no image for " . $artist_xml->name . " artist", 'warning' );
  //END ARTIST IMAGE

  //node_submit($newNode);
  node_save($newNode);

  if ($newNode->nid){
    watchdog('tonspion_7digital',
      'New artist added (%nid): %name',
      array( '%nid' => $newNode->nid, '%name' => $newNode->title ), 
      WATCHDOG_INFO);     
  }
  else
  {
    watchdog('tonspion_7digital',
      'Could not add artist: %name',
      array('%name' => $artist_xml->title), 
      WATCHDOG_WARNING); 

  }

  return $newNode->nid;
}


function _update_image_artist_from_7D (&$artist_node, $artist_xml) {

  if ( !@$artist_xml->image &&  !@$artist_xml->sortName ) {
	   $artist_xml = _artist_info_request_from_7D( $artist_xml );
  }

  /****************
   * ARTIST IMAGE *
   * **************
   */
  if (@$artist_xml->image) {
	  //preparing 200px image form xml
	  //$artist_xml->image = preg_replace("(_150)", "_200", $artist_xml->image, -1);

	  $url = $artist_xml->image;
	  $file_info = system_retrieve_file($url, 'public://', TRUE);
	  if($file_info->fid){
		$artist_node->field_artist_image['und']['0']['fid'] = $file_info->fid;
	  }

  }else _my_drush_log( "no image for " . $artist_xml->name . " artist", 'warning' );
  //END ARTIST IMAGE

  //save processing time
  $artist_node->field_artist_sdid_update_time['und'][0]['value']       = time();
  $artist_node->field_artist_sdid_update_time['und'][0]['timezone']    = "Europe/Berlin";
  $artist_node->field_artist_sdid_update_time['und'][0]['timezone_db'] = "UTC";
  $artist_node->field_artist_sdid_update_time['und'][0]['date_type']   = "datestamp";

  //node_submit($newNode);
  node_save($artist_node);

  watchdog('tonspion_7digital',
    'Artist image updated. Nid: %nid',
    array('%nid' => $newNode->nid), 
    WATCHDOG_INFO); 

  return $artist_node->nid;
}


function _artist_info_request_from_7D ( $artist_xml ) {
   //DEBUG
   //print_r($artist_xml);

   /*********************************
   * API REQUEST TO GET ARTIST INFO *
   **********************************
   */

    $client = tonspion_7digital_client();
    $artist = $client->getArtistService();

	//DEBUG:
	if (! is_numeric( $artist_xml->{"@attributes"}->id )) {
	  watchdog('tonspion_7digital', 'Error: no id found for artist<br/> %e', $variables = array('%e'=>$e), WATCHDOG_WARNING);
	  return;
	}

	//Service call
	try {
	  $result_xml = $artist->details(
        array(
          'country'     => TONSPION_7DIGITAL_LANGUAGE,
          'artistId'    => $artist_xml->{"@attributes"}->id,
		  'imageSize'   => 300
      ));

	  //DEBUG
	  //print_r($result_xml);die("art");

	  } catch (Exception $e) {
		watchdog('tonspion_7digital', 'Exception with API service getting artist info: <br/> %e', $variables = array('%e'=>$e), WATCHDOG_WARNING);
	  }



    $artist_xml = @$result_xml->xpath("//artist")[0];
    $artist_xml = json_decode(json_encode( $artist_xml ));

	//DEBUG:
    //print_r($artist_xml); die();

  /*
  *********************
  *
  *  END API  REQUEST *
  * *******************/

  //TODO: watchdog if $artist_xml isn't object
  return $artist_xml;
}


function _get_release_tags_from_7D( $release_xml ) {

  /*******************************************
   * API REQUEST TO GET RELEASE TAGS [GENRE] *
   *******************************************
   */
  $client = tonspion_7digital_client();

  //API Service
  $service = $client->getReleaseService();

  //Request options
  $options = array(
      'releaseId'         =>  (int) $release_xml->{"@attributes"}->id,
      'country'           => TONSPION_7DIGITAL_LANGUAGE,
      'page'              => 1,
      'pageSize'          => 10,
  );

  //Service call
  try {

    $tags = @$service->tags( $options );

    //DEBUG
	//print_r($tags);die();
  } catch (Exception $e) {
    watchdog('tonspion_7digital', 'Exception with API service getting album tags: <br/> %e', $variables = array('%e'=>$e), WATCHDOG_WARNING);

  }
  /*
  *********************
  *
  *  END API  REQUEST *
  * *******************/

  return $tags;
}
