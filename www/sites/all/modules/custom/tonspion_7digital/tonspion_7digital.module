<?php

/**
 * @file
 * Tonspion 7Digital - content from 7digital integration
 *
 * It requires Composer Manager to meet dependencies like external library qquemener/7digital-client. As
 * other composer libraries they must be in composer vendor file, by default sites/all/vendor.
 *
 * Suggested configuration (/admin/config/system/composer-manager/settings):
 *   Vendor directory: ../vendor
 *   Composer file directory: public://composer
 * 
 * At the time Composer Manager Drush is not working proprrly, so when getting dependencies you need to
 * regerate Composer file (by default in public://composer, that is in sites/default/files/composer/composer.json)
 * though Composer Manager
 * admin and run "php composer.phar update" in that directory.
 *
 * See Composer Manager project page for more info: https://drupal.org/project/composer_manager.
 *
 * @author  jorge.maestre@devtopia.coop
 * @author leandro.vazquez@devtopia.coop
 */

require_once 'includes/tonspion_7digital.inc';
require_once 'tonspion_7digital.queues.inc';

define( 'TONSPION_7DIGITAL_DEFAULT_CONSUMER',   '7digitalClips');
define( 'TONSPION_7DIGITAL_DEFAULT_SECRET',     'sa5ra9AwReswUf89');
define( 'TONSPION_7DIGITAL_PAGE_SIZE',          500);
define( 'TONSPION_7DIGITAL_UID',                1);
define( 'TONSPION_7DIGITAL_LANGUAGE',           'DE');

if ( function_exists('drush_main') )            define( 'IS_DRUSH', '1');

/**
 * Implements hook_help().
 */
function tonspion_7digital_help($path, $arg) {

  switch ($path) {
    case 'admin/help#tonspion_7digital':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The Tonspion 7Digital module allows to import artits and releases from 7Digital site into Tonspion drupal content types') . '</p>';
      $output .= '<p>'.t('It requires Composer Manager to meet dependencies like external library qquemener/7digital-client. As other composer libraries they must be in composer vendor file, by default sites/all/vendor.').'</p>';
      $output .= '<p>'.t('See Composer Manager project page for more info.').'</p>';
      $output .= '<h4>'.t('Installing Composer').'</h4>';
      $output .= '<ol>';
      $output .= '<li>'.t('Enable module Tonspion 7digital').'</li>';
      $output .= '<li>'.t('json file is installed in sites/default/files/composer').'</li>';
      $output .= '<li>'.t('Go to <i>sites/default/files/composer</i> and install composer executing: <i>curl -sS https://getcomposer.org/installer | php -d detect_unicode=off</i>').'</li>';
      $output .= '<li>'.t('Execute <i>php composer.phar install</i>').'</li>';
      $output .= '<li>'.t('Execute <i>drush composer-manager update</i>').'</li>';
      $output .= '<li>'.t('Check at <i>admin/config/system/composer-manager</i> if you have all packages installed').'</li>';
      $output .= '</ol>';
      $output .= '<p>'.t('See Composer Manager project page for more info.').'</p>';
      return $output;
  }

}


/**
 * Implement of hook_menu().
 */
function tonspion_7digital_menu() {

  $items = array();

  $base = drupal_get_path('module', 'tonspion_7digital');

  // 7Digital admin settings
  $items['admin/config/services/7digital'] = array(
    'title'             => '7 Digital',
    'description'       => 'Check status of 7Digital API and configure access to it.',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('tonspion_7digital_settings'),
    'access arguments'  => array('administer site configuration'),
    'file'              => 'tonspion_7digital.admin.inc',

  );

  //**** ARTIST ********

  // API Test page
  $items['admin/content/tonspion/7digital'] = array(
    'title'             => '7Digital',
    'description'       => '7Digital content manager',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('tonspion_7digital_import_form'),
    'access arguments'  => array('administer site configuration'),
    'file'              => 'tonspion_7digital.admin.inc',
  );

  // 7digital Import settings
  $items['admin/content/tonspion/7digital/import'] = array(
    'title'             => 'Import settings',
    'description'       => '7Digital content manager',
    'type'              => MENU_DEFAULT_LOCAL_TASK,
    'weight'            => 0,
  );

  // API Test: artist/search
  $items['admin/content/tonspion/7digital/artist-search'] = array(
    'title'             => 'artist/search',
    'description'       => 'Test artist/search method',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('tonspion_7digital_artist_search_form'),
    'access arguments'  => array('administer site configuration'),
    'file'              => 'tonspion_7digital.admin.inc',
    'type'              => MENU_LOCAL_TASK,
    'weight'            => 1,
  );

  // API Test: artist/details
  $items['admin/content/tonspion/7digital/artist-details'] = array(
    'title'             => 'artist/details',
    'description'       => 'Test artist/details method',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('tonspion_7digital_artist_details_form'),
    'access arguments'  => array('administer site configuration'),
    'file'              => 'tonspion_7digital.admin.inc',
    'type'              => MENU_LOCAL_TASK,
    'weight'            => 2,
  );

   // API Test: artist/details
  $items['admin/content/tonspion/7digital/artist-tags'] = array(
    'title'             => 'artist/tags',
    'description'       => 'Test artist/tags method',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('tonspion_7digital_artist_tags_form'),
    'access arguments'  => array('administer site configuration'),
    'file'              => 'tonspion_7digital.admin.inc',
    'type'              => MENU_LOCAL_TASK,
    'weight'            => 3,
  );

  //**** RELEASES ********

  // API Test: releases/search
  $items['admin/content/tonspion/7digital/release-search'] = array(
    'title'             => 'release/search',
    'description'       => 'Test release/search method',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('tonspion_7digital_release_search_form'),
    'access arguments'  => array('administer site configuration'),
    'file'              => 'tonspion_7digital.admin.inc',
    'type'              => MENU_LOCAL_TASK,
    'weight'            => 9,
  );

  // API Test: releases/bydate
  $items['admin/content/tonspion/7digital/release-bydate'] = array(
    'title'             => 'release/bydate',
    'description'       => 'Test release/bydate method',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('tonspion_7digital_release_bydate_form'),
    'access arguments'  => array('administer site configuration'),
    'file'              => 'tonspion_7digital.admin.inc',
    'type'              => MENU_LOCAL_TASK,
    'weight'            => 10,
  );

  // API Test: releases/details
  $items['admin/content/tonspion/7digital/release-details'] = array(
    'title'             => 'release/details',
    'description'       => 'Test release/details method',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('tonspion_7digital_release_details_form'),
    'access arguments'  => array('administer site configuration'),
    'file'              => 'tonspion_7digital.admin.inc',
    'type'              => MENU_LOCAL_TASK,
    'weight'            => 11,
  );

  // API Test: releases/tags
  $items['admin/content/tonspion/7digital/release-tags'] = array(
    'title'             => 'release/tags',
    'description'       => 'Test release/tags method',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('tonspion_7digital_release_tags_form'),
    'access arguments'  => array('administer site configuration'),
    'file'              => 'tonspion_7digital.admin.inc',
    'type'              => MENU_LOCAL_TASK,
    'weight'            => 12,
  );

  return $items;
}



/**
 * hook_cron(): Perform periodic actions.
 */
function tonspion_7digital_cron() {

  // Short-running operation example, not using a queue:
  // Delete all expired records since the last cron run.
  // $expires = variable_get('mymodule_cron_last_run', REQUEST_TIME);
  // db_delete('mymodule_table')
  //   ->condition('expires', $expires, '>=')
  //   ->execute();
  // variable_set('mymodule_cron_last_run', REQUEST_TIME);


  // Populate new releases from 7Digital
  // get new releases from 7Digital API

  $response = tonspion_7digital_new_releases();

  if(!empty($response)){
    $queue = DrupalQueue::get('tonspion_7digital_releases');
    $releases = $response->xpath("//release");

    if (!is_array($releases)) {
		  _my_drush_log( "no new releases found" , 'warning' );
		  return;
    }

    $queue->createQueue();

    foreach ($releases as $release) {
	   $release = json_decode(json_encode( $release )); //@TODO: UTF-8 conversion here, is it a problem?
       $queue->createItem( $release );
    }

    watchdog('tonspion_7digital', '%count new releases found since yesterday and added to queue (tonspion_7digital_releases).', 
      array( '%count'=>count($releases) ), 
      WATCHDOG_INFO);    
  }

  // Check for albums updated in order to update drupal albums

   $response_albums = tonspion_7digital_new_albums_update();

   if(!empty($response_albums)){
	   $queue = DrupalQueue::get('tonspion_7digital_albums_update');
	   $releases = $response_albums->xpath("//release");

	   foreach ($releases as $release) {
  		 $release = json_decode(json_encode( $release ));
  		 $queue->createItem($release);
	   }

     watchdog('tonspion_7digital', '%count new albums releases found and added to queue (tonspion_7digital_albums_update).', 
       array( '%count'=>count($releases) ), 
       WATCHDOG_INFO);     
   }

   //FIXME: ARTIST UPDATE DISABLED FOR NOW!!

   #$response_artists = tonspion_7digital_existing_artists_update();

  // get artists to refresh in drupal
  /*
  if(!empty($response_artists)){
   $queue = DrupalQueue::get('tonspion_7digital_artists_update');
   $releases = $response_artists->xpath("//artist");
	   foreach ($releases as $release) {
		 $release = json_decode(json_encode( $release ));
		 $queue->createItem($release);
	   }
  }
  */
}


/**
 * hook_cron_queue_info(): Declare queues holding items that need to be run periodically.
 *
 * While there can be only one hook_cron() process running at the same time,
 * there can be any number of processes defined here running. Because of
 * this, long running tasks are much better suited for this API. Items queued
 * in hook_cron() might be processed in the same cron run if there are not many
 * items in the queue, otherwise it might take several requests, which can be
 * run in parallel.
 *
 * @return
 *   An associative array where the key is the queue name and the value is
 *   again an associative array. Possible keys are:
 *   - 'worker callback': The name of the function to call. It will be called
 *     with one argument, the item created via DrupalQueue::createItem() in
 *     hook_cron().
 *   - 'time': (optional) How much time Drupal should spend on calling this
 *     worker in seconds. Defaults to 15.
 *
 * @see hook_cron()
 * @see hook_cron_queue_info_alter()
 */
function tonspion_7digital_cron_queue_info() {
  //@TODO: temporal

  //FIXME: DEBUG
  /*
  $queue = DrupalQueue::get('tonspion_7digital_releases');
  $queue->deleteQueue();

  $queue = DrupalQueue::get('tonspion_7digital_albums_update');
  $queue->deleteQueue();

  $queue = DrupalQueue::get('tonspion_7digital_artists_update');
  $queue->deleteQueue();
  */


  $queues['tonspion_7digital_releases'] = array(
    'worker callback' => 'tonspion_7digital_worker_release',
    'time' => variable_get('7D_val_new_releases_n_segs_4_releases','60'),
  );

   $queues['tonspion_7digital_albums_update'] = array(
     'worker callback' => 'tonspion_7digital_worker_old_album',
     'time' => variable_get('7D_val_albums_n_segs_4_album','60'),
   );
/*
   $queues['tonspion_7digital_artists_update'] = array(
     'worker callback' => 'tonspion_7digital_worker_old_artist',
     'time' => variable_get('7D_val_artists_n_segs_4_artists','60'),
   );
*/
  return $queues;
}

/**
 * Implements hook_block_info().
 */
function tonspion_7digital_block_info() {
  $blocks = array();
  $blocks['tonspion_7digital_player'] = array(
    'info' => t('Tonspion 7digital: player'),
    'cache' => DRUPAL_CACHE_PER_PAGE
  );

  return $blocks;
}


/**
 * Implements hook_block_view().
 */
function tonspion_7digital_block_view($delta='') {
  $block = array();

  switch($delta) {
    case 'tonspion_7digital_player' :
      // Adding player only in album node page
      $node = menu_get_object();

      // Check if we have an album and it has a 7ditial ID
      if ( $node && $node->type == 'album' && !empty($node->field_album_sevendigital_id) && $node->field_album_sevendigital_id['und'][0]['value'] != '' ) {
        //dsm($node);

        $album_7d_id = $node->field_album_sevendigital_id['und'][0]['value'];

        // Prepare the settings for player
        $settings = array(
          'tonspion_7digital_player' => array(
              'album_id' => $album_7d_id,
              'oauth_consumer_key' => '7digitalClips', //variable_get('tonspion_7digital__consumer_key', TONSPION_7DIGITAL_DEFAULT_CONSUMER),
              'audio_wrapper_id' => 'sevendigital-player-wrapper',
              'audio_player_id' => 'sevendigital-player',
              'autoplay' => 0,
          )
        );
        drupal_add_css(drupal_get_path('module', 'tonspion_7digital') . '/css/tonspion_7digital.css');
        drupal_add_library('system', 'ui');
        drupal_add_library('system', 'ui.slider');
        drupal_add_js($settings, 'setting');
        drupal_add_js(drupal_get_path('module', 'tonspion_7digital') . '/js/tonspion_7digital_player.js');

        $block['content'] = theme('tonspion_7digital_album_player',array( 'album'=>$node , 'artist' => node_load($node->field_ref_artist['und'][0]['target_id']) ));
      } else if ($node && $node->type == 'album') {
        watchdog('tonspion_7digital', '7digital player not showing because the 7digital ID is missing on node ' . $node->nid , null , WATCHDOG_ALERT);
        //drupal_set_message(t('7digital player not showing because the 7digital ID is missing on this node'),'warning');
        drupal_add_css(drupal_get_path('module', 'tonspion_7digital') . '/css/tonspion_7digital.css');
        $block['content'] = theme('tonspion_7digital_album_player',array( 'album'=>$node , 'artist' => node_load($node->field_ref_artist['und'][0]['target_id']) ));
      } else {
        $block['content'] = '';
      }

      break;
  }

  return $block;
}

/**
 * Implements hook_theme().
 */
function tonspion_7digital_theme() {
  return array(
    'tonspion_7digital_album_player' => array(
       'file' => 'tonspion_7digital.theme.inc',
       'variables' => array(
          'mp3'     => NULL,
          'album'   => NULL,
          'artist'  => NULL,
        )
    )
  );
}

/**
 * Private helper function that logs when using drush.
 */
function _my_drush_log ($msg, $status) {
	if (IS_DRUSH && function_exists ( 'drush_log' )) {
		drush_log($msg, $status);
	}
}
