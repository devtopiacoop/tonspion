<?php
//define('PARQUES_SAMPLE', 0);

/**
 * Tonspion 7Digital settings.
 */
function tonspion_7digital_settings() {

  $form = array();

  $form['#prefix']  = '<h2>'.t('7Digital import settings').'</h2>';

  $form['tonspion_7digital__consumer_key'] = array(
    '#type' => 'textfield',
    '#title' => t('7Digital consumer key'),
    '#description' => t('Please input the consumer key.'),
    '#default_value' => variable_get('tonspion_7digital__consumer_key', TONSPION_7DIGITAL_DEFAULT_CONSUMER),
  );

  $form['tonspion_7digital__consumer_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('7Digital consumer secret key'),
    '#description' => t('Please input the consumer secret key needed for premium services.'),
    '#default_value' => variable_get('tonspion_7digital__consumer_secret', TONSPION_7DIGITAL_DEFAULT_SECRET),
  );

  return system_settings_form($form);
}


/**
 * Test 7Digital API method: artist/search.
 *
 * @see tonspion_7digital_artist_search_form_submit()
 *
 * @ingroup forms
 */
function tonspion_7digital_artist_search_form($form, &$form_state) {

  global $language;

  $form = _tonspion_7digital_form_data();

  $form['search'] = array(
    '#title' => 'Artist name',
    '#type' => 'textfield',
    '#default_value' => 'queen',
    '#description' => t('Artist name to search'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 10,
  );

  $form = array_merge ($form, _tonspion_7digital_artist_form_pager());

  if (isset($form_state['values']['results'])) {
    $form['submission'] = array(
      '#markup' => $form_state['values']['results'],
      '#weight' => 50,
    );
  }

  return $form;

}


/**
 * Search artist given a name
 */
function tonspion_7digital_artist_search_form_submit($form, &$form_state) {

  global $language;

  $client = tonspion_7digital_client();

  //Services track, release and tag are also accessible the same way
  $artist = $client->getArtistService();

  // Use the method name as described in the 7digital api documentation
  $results = $artist->search(
    array(
      'country'     => TONSPION_7DIGITAL_LANGUAGE,
      'q'           => $form['search']['#value'],
      'page'        => $form['pager_page']['#value'],
      'pageSize'    => $form['pager_pageSize']['#value'],
      )
  );

  $search = $results->xpath("//searchResults")[0];

  $form_state['rebuild'] = TRUE;

  $output  = '<h3>'. t('Search params') . '</h3>';
  $output .= '<p>';
  $output .= 'page: ' . $search->page . '<br/>';
  $output .= 'pageSize: ' . $search->pageSize . '<br/>';
  $output .= 'totalItems: ' . $search->totalItems . '<br/>';
  $output .= '</p>';

  $output .= '<h3>'. t('Search results') . '</h3>';
  foreach($results->xpath("//searchResult") as $search_result){
    $output .= '<p>';
    $output .= 'Score:' . $search_result->score . '<br/>';
    $output .= 'Type:' . $search_result->type . '<br/>';
    $item = $search_result->xpath($search_result->type)[0];

    $output .= 'Id: ' . $item['id'] . '<br/>';
    $output .= _dump_properties($item);
    // foreach($item->children() as $child){
    //   $output .= $child->getName() . ': ' . $child . '<br/>';
    // }
    $output .= '</p>';
    $output .= '<hr/>';
  }

  $form_state['values']['results'] = $output;

}


/**
 * Test 7Digital API method: artist/details.
 *
 * @see tonspion_7digital_artist_details_form_submit()
 *
 * @ingroup forms
 */
function tonspion_7digital_artist_details_form($form, &$form_state) {

  global $language;

  $form = _tonspion_7digital_form_data();

  $form['artist_id'] = array(
    '#title'          => 'Artist id',
    '#type'           => 'textfield',
    '#default_value'  => '204',
    '#description'    => t('Artist id to search'),
  );

  $form['submit'] = array(
    '#type'           => 'submit',
    '#value'          => t('Submit'),
    '#weight'         => 10,
  );

  if (isset($form_state['values']['results'])) {
    $form['submission'] = array(
      '#markup'       => $form_state['values']['results'],
      '#weight'       => 50,
    );
  }

  return $form;

}


/**
 * Get artists details
 */
function tonspion_7digital_artist_details_form_submit($form, &$form_state) {

  global $language;

  $client = tonspion_7digital_client();

  //Services track, release and tag are also accessible the same way
  $artist = $client->getArtistService();

  // Use the method name as described in the 7digital api documentation
  $results = $artist->details(
    array(
      'country'     => TONSPION_7DIGITAL_LANGUAGE,
      'artistId'    => $form['artist_id']['#value'],
      )
  );

  $artist = $results->xpath("//artist")[0];

  $form_state['rebuild'] = TRUE;

  $output  = '<h3>'. t('Results') . '</h3>';
  $output .= '<p>';
  $output .= _dump_properties($artist);
  $output .= '</p>';


  $form_state['values']['results'] = $output;

}



/**
 * Test 7Digital API method: artist/tags.
 *
 * @see tonspion_7digital_artist_tags_form_submit()
 *
 * @ingroup forms
 */
function tonspion_7digital_artist_tags_form($form, &$form_state) {

  global $language;

  $form = _tonspion_7digital_form_data();

  $form['artist_id'] = array(
    '#title'          => 'Artist id',
    '#type'           => 'textfield',
    '#default_value'  => '204',
    '#description'    => t('Artist id to search'),
  );

  $form['submit'] = array(
    '#type'           => 'submit',
    '#value'          => t('Submit'),
    '#weight'         => 10,
  );

  if (isset($form_state['values']['results'])) {
    $form['submission'] = array(
      '#markup'       => $form_state['values']['results'],
      '#weight'       => 50,
    );
  }

  return $form;

}


/**
 * Get artists tags
 */
function tonspion_7digital_artist_tags_form_submit($form, &$form_state) {

  global $language;

  $client = tonspion_7digital_client();

  //Services track, release and tag are also accessible the same way
  $artist = $client->getArtistService();

  // Use the method name as described in the 7digital api documentation
  $results = $artist->tags(
    array(
      'country'     => TONSPION_7DIGITAL_LANGUAGE,
      'artistId'    => $form['artist_id']['#value'],
      )
  );

  $form_state['rebuild'] = TRUE;

  $output  = '<h3>'. t('Results') . '</h3>';
  foreach($results->xpath("//tag") as $item){
    $output .= '<p>';
    $output .= 'Id: ' . $item['id'] . '<br/>';
    $output .= _dump_properties($item);
    $output .= '</p>';
  }
  $output .= '</p>';


  $form_state['values']['results'] = $output;

}




/**
 * Test 7Digital API method: release/details.
 *
 * @see tonspion_7digital_release_details_form_submit()
 *
 * @ingroup forms
 */
function tonspion_7digital_release_details_form($form, &$form_state) {

  global $language;

  $form = _tonspion_7digital_form_data();

  $form['release_id'] = array(
    '#title'          => 'Release id',
    '#type'           => 'textfield',
    '#default_value'  => '1435450',
    '#description'    => t('Release id to search'),
  );

  $form['submit'] = array(
    '#type'           => 'submit',
    '#value'          => t('Submit'),
    '#weight'         => 10,
  );

  if (isset($form_state['values']['results'])) {
    $form['submission'] = array(
      '#markup'       => $form_state['values']['results'],
      '#weight'       => 50,
    );
  }

  return $form;

}



/**
 * Get artists details
 */
function tonspion_7digital_release_details_form_submit($form, &$form_state) {

  global $language;

  $client = tonspion_7digital_client();

  //Services track, release and tag are also accessible the same way
  $artist = $client->getReleaseService();

  // Use the method name as described in the 7digital api documentation
  $results = $artist->details(
    array(
      'country'     => TONSPION_7DIGITAL_LANGUAGE,
      'releaseId'   => $form['release_id']['#value'],
      )
  );

  $artist = $results->xpath("//release")[0];

  $form_state['rebuild'] = TRUE;

  $output  = '<h3>'. t('Results') . '</h3>';
  $output .= '<p>';
  $output .= _dump_properties($artist);
  $output .= '</p>';


  $form_state['values']['results'] = $output;

}



/**
 * Test 7Digital API method: release/search.
 *
 * @see tonspion_7digital_release_search_form_submit()
 *
 * @ingroup forms
 */
function tonspion_7digital_release_search_form($form, &$form_state) {

  global $language;

  $form = _tonspion_7digital_form_data();

  $form['search'] = array(
    '#title' => 'Release name',
    '#type' => 'textfield',
    '#default_value' => 'a night at the opera',
    '#description' => t('Release name to search'),
  );

  $form['submit'] = array(
    '#type'           => 'submit',
    '#value'          => t('Submit'),
    '#weight'         => 10,
  );

  $form = array_merge ($form, _tonspion_7digital_artist_form_pager());

  if (isset($form_state['values']['results'])) {
    $form['submission'] = array(
      '#markup' => $form_state['values']['results'],
      '#weight' => 50,
    );
  }

  return $form;

}


/**
 * Search release given a name
 */
function tonspion_7digital_release_search_form_submit($form, &$form_state) {

  global $language;

  $client = tonspion_7digital_client();

  //Services track, artists and tag are also accessible the same way
  $service = $client->getReleaseService();

  // Use the method name as described in the 7digital api documentation
  $results = $service->search(
    array(
      'country'     => TONSPION_7DIGITAL_LANGUAGE,
      'q'           => $form['search']['#value'],
      'page'        => $form['pager_page']['#value'],
      'pageSize'    => $form['pager_pageSize']['#value'],
      )
  );

  $search = $results->xpath("//searchResults")[0];

  $form_state['rebuild'] = TRUE;

  $output  = '<h3>'. t('Search params') . '</h3>';
  $output .= '<p>';
  $output .= 'page: ' . $search->page . '<br/>';
  $output .= 'pageSize: ' . $search->pageSize . '<br/>';
  $output .= 'totalItems: ' . $search->totalItems . '<br/>';
  $output .= '</p>';

  $output .= '<h3>'. t('Search results') . '</h3>';
  foreach($results->xpath("//searchResult") as $search_result){
    $output .= '<p>';
    $output .= 'Score:' . $search_result->score . '<br/>';
    $output .= 'Type:' . $search_result->type . '<br/>';
    $item = $search_result->xpath($search_result->type)[0];

    $output .= 'Id: ' . $item['id'] . '<br/>';
    $output .= _dump_properties($item);
    // foreach($item->children() as $child){
    //   $output .= $child->getName() . ': ' . $child . '<br/>';
    // }
    $output .= '</p>';
    $output .= '<hr/>';
  }

  $form_state['values']['results'] = $output;

}








/**
 * Test 7Digital API method: release/bydate.
 *
 * @see tonspion_7digital_release_bydate_form_submit()
 *
 * @ingroup forms
 */
function tonspion_7digital_release_bydate_form($form, &$form_state) {

  global $language;

  $form = _tonspion_7digital_form_data();

  $form['fromDate'] = array(
    '#title'          => 'fromDate',
    '#type'           => 'textfield',
    '#size'           => 8,
    '#default_value'  => date('Ymd', strToTime('-1 month')),
    '#description'    => t('Date format is YYYYMMDD. The first day for which to include data. The default date is today\'s date.'),
  );

  $form['toDate'] = array(
    '#title'          => 'toDate',
    '#type'           => 'textfield',
    '#size'           => 8,
    '#default_value'  => date("Ymd"),
    '#description'    => t('Date format is YYYYMMDD. The last day for which to include data. The default date is today\'s date.'),
  );

  $form['submit'] = array(
    '#type'           => 'submit',
    '#value'          => t('Submit'),
    '#weight'         => 10,
  );

  $form = array_merge ($form, _tonspion_7digital_artist_form_pager());

  if (isset($form_state['values']['results'])) {
    $form['submission'] = array(
      '#markup' => $form_state['values']['results'],
      '#weight' => 50,
    );
  }

  return $form;

}


/**
 * Search artist given a name
 */
function tonspion_7digital_release_bydate_form_submit($form, &$form_state) {

  $client = tonspion_7digital_client();

  //Services track, release and tag are also accessible the same way
  $service = $client->getReleaseService();

  $options = array(
      'country'           => TONSPION_7DIGITAL_LANGUAGE,
      'page'              => $form['pager_page']['#value'],
      'pageSize'          => $form['pager_pageSize']['#value'],
  );

  if(!empty($form['fromDate']['#value']))
    $options['fromDate']  = $form['fromDate']['#value'];

  if(!empty($form['toDate']['#value']))
    $options['toDate']    = $form['toDate']['#value'];

  // Use the method name as described in the 7digital api documentation
  $results = $service->bydate( $options );

  $releases = $results->xpath("//releases")[0];

  $form_state['rebuild'] = TRUE;

  $output  = '<h3>'. t('Params') . '</h3>';
  $output .= '<p>';
  $output .= 'page: ' . $releases->page . '<br/>';
  $output .= 'pageSize: ' . $releases->pageSize . '<br/>';
  $output .= 'totalItems: ' . $releases->totalItems . '<br/>';
  $output .= '</p>';

  $output .= '<h3>'. t('Results') . '</h3>';
  foreach($results->xpath("//release") as $item){
    $output .= '<p>';
    $output .= 'Id: ' . $item['id'] . '<br/>';
    $output .= _dump_properties($item);
    $output .= '</p>';
    $output .= '<hr/>';
  }

  $form_state['values']['results'] = $output;

}


/**
 * Test 7Digital API method: release/tags.
 *
 * @see tonspion_7digital_release_tags_form_submit()
 *
 * @ingroup forms
 */
function tonspion_7digital_release_tags_form($form, &$form_state) {

  global $language;

  $form = _tonspion_7digital_form_data();

  $form['release_id'] = array(
    '#title'          => 'Release id',
    '#type'           => 'textfield',
    '#default_value'  => '1435450',
    '#description'    => t('Release id to search'),
  );

  $form['submit'] = array(
    '#type'           => 'submit',
    '#value'          => t('Submit'),
    '#weight'         => 10,
  );

  $form = array_merge ($form, _tonspion_7digital_artist_form_pager());

  if (isset($form_state['values']['results'])) {
    $form['submission'] = array(
      '#markup'       => $form_state['values']['results'],
      '#weight'       => 50,
    );
  }

  return $form;

}


/**
 * Get release tags
 */
function tonspion_7digital_release_tags_form_submit($form, &$form_state) {

  global $language;

  $client = tonspion_7digital_client();

  //Services track, release and tag are also accessible the same way
  $service = $client->getReleaseService();

  $options = array(
      'releaseId'         => $form['release_id']['#value'],
      'country'           => TONSPION_7DIGITAL_LANGUAGE,
      'page'              => $form['pager_page']['#value'],
      'pageSize'          => $form['pager_pageSize']['#value'],
  );


  // Use the method name as described in the 7digital api documentation
  $results = $service->tags( $options );

  $tags = $results->xpath("//tags")[0];

  $form_state['rebuild'] = TRUE;

  $output  = '<h3>'. t('Params') . '</h3>';
  $output .= '<p>';
  $output .= 'page: ' . $tags->page . '<br/>';
  $output .= 'pageSize: ' . $tags->pageSize . '<br/>';
  $output .= 'totalItems: ' . $tags->totalItems . '<br/>';
  $output .= '</p>';

  $output .= '<h3>'. t('Results') . '</h3>';
  foreach($results->xpath("//tag") as $item){
    $output .= '<p>';
    $output .= 'Id: ' . $item['id'] . '<br/>';
    $output .= _dump_properties($item);
    $output .= '</p>';
  }

  $form_state['values']['results'] = $output;

}




/**
 * Pager form helper
 *
 * @ingroup forms
 */
function _tonspion_7digital_artist_form_pager() {

  $form['pager_page'] = array(
    '#type' => 'textfield',
    '#title' => t('page'),
    '#default_value' => 1,
    '#size' => 3
  );
  $form['pager_pageSize'] = array(
    '#type' => 'textfield',
    '#title' => t('pageSize'),
    '#default_value' => 10,
    '#size' => 3
  );

  return $form;

}


/**
 * Data form helper
 *
 * @ingroup forms
 */
function _tonspion_7digital_form_data() {

  $form['#prefix'] = '<h3>' . t('Parameters') . '</h3>';
  $form['#prefix'] .= '<p>' . t('Costumer name: ') . variable_get('tonspion_7digital__consumer_key', TONSPION_7DIGITAL_DEFAULT_CONSUMER) ;
  $form['#prefix'] .= '<br/>' . t('Costumer secret: ') . variable_get('tonspion_7digital__consumer_secret', TONSPION_7DIGITAL_DEFAULT_SECRET) ;
  $form['#prefix'] .= '<br/>' . t('Country: ') .  TONSPION_7DIGITAL_LANGUAGE . '</p>';

  return $form;

}




/**
 * 7Digital import content settings
 *
 * @see composer_manager_settings_form_validate()
 *
 * @ingroup forms
 */
function tonspion_7digital_import_form($form, &$form_state) {

  $form = array();

  $form['7D_val_new_releases'] = array(
	  '#type' => 'fieldset',
	  '#title' => t('New Releases'),
	  '#collapsible' => FALSE,
	  '#collapsed' => FALSE,
  );

  $form['7D_val_new_releases']['7D_val_new_releases_n_segs_4_releases'] = array(
    '#title'          => 'Number of seconds to run this queue',
    '#type'           => 'textfield',
    '#element_validate' => array('element_validate_integer_positive'),
    '#default_value'  => variable_get('7D_val_new_releases_n_segs_4_releases','60'),
  );

  $form['7D_val_new_releases']['tonspion_7digital_releases_last_check'] = array(
    '#title'          => 'Period to fetch releases from 7Digital',
    '#type'           => 'textfield',
    '#default_value'  => variable_get('tonspion_7digital_releases_last_check', '7 days'),
    '#description'    => '"27 days" or "1 month" are valid values. Only positive periods.',
  );

  $form['7D_val_albums_update'] = array(
	  '#type' => 'fieldset',
	  '#title' => t('Albums Update'),
	  '#collapsible' => FALSE,
	  '#collapsed' => FALSE,
	);


  $form['7D_val_albums_update']['7D_val_albums_n_segs_4_album'] = array(
    '#title'          => 'Number of seconds to run this queue',
    '#type'           => 'textfield',
    '#element_validate' => array('element_validate_integer_positive'),
    '#default_value'  => variable_get('7D_val_albums_n_segs_4_album','60'),
  );

  $form['7D_val_albums_update']['tonspion_7digital_albums_refresh'] = array(
    '#title'          => 'Period to fetch albums from 7Digital',
    '#type'           => 'textfield',
    '#default_value'  => variable_get('tonspion_7digital_albums_refresh','1 year'),
    '#description'    => '"27 days" or "1 month" are valid values. Only positive periods.',
  );


  return system_settings_form($form);

}


/**
 * Dump object properties
 *
 * @return void
 * @author
 **/
function _dump_properties($obj, $sep="<br/>", $indent=0)
{

  $output="";

  foreach($obj as $key => $value) {
    //dsm(count($value->children()));
    if(count($value->children())){
      $indent++;
      $output .= _dump_properties($value, $sep, $indent);
      $indent--;
    }
    else
    {
      $output .= str_repeat("&nbsp;&nbsp;&nbsp;",$indent)."$key: $value".$sep;      
    }
  }

  return $output;
}
