
/**
 * @file tonspion_7digital/js/tonspion_7digital.js
 */

(function ($) {

Drupal.tonspion_7digital_player = {};

Drupal.tonspion_7digital_player.song;

Drupal.behaviors.tonspion_7digital_player = {
  attach: function (context, settings) {
    // Check the browser to see if it supports html5 video.
    var audio = document.createElement('audio');
    var html5 = audio.canPlayType ? true : false;

    

    // If it has video, does it support the correct codecs?
    if (!html5) {
      alert('Your browser is not ready for HTML5 player')
    } else {
      if ( html5 && Drupal.settings && Drupal.settings.tonspion_7digital_player) {
        
        // Grab tracks
        Drupal.tonspion_7digital_player.insertTracks();

        // play click
        $('.play').click(function (e) {
            e.preventDefault();

            Drupal.tonspion_7digital_player.playAudio();
        });

        // pause click
        $('.pause').click(function (e) {
            e.preventDefault();

            Drupal.tonspion_7digital_player.stopAudio();
        });

        // forward click
        $('.fwd').click(function (e) {
            e.preventDefault();

            Drupal.tonspion_7digital_player.stopAudio();

            var next = $('.playlist li.active').next();
            if (next.length == 0) {
                next = $('.playlist li:first-child');
            }
            Drupal.tonspion_7digital_player.initAudio(next);
        });

        // rewind click
        $('.rew').click(function (e) {
            e.preventDefault();

            Drupal.tonspion_7digital_player.stopAudio();

            var prev = $('.playlist li.active').prev();
            if (prev.length == 0) {
                prev = $('.playlist li:last-child');
            }
            Drupal.tonspion_7digital_player.initAudio(prev);
            
        });

        // show playlist
        $('.pl').click(function (e) {
            e.preventDefault();

            $('.playlist').fadeIn(300);
        });

        // playlist elements - click
        $('.playlist li').click(function () {
            Drupal.tonspion_7digital_player.stopAudio();
            Drupal.tonspion_7digital_player.initAudio($(this));
        });

// empty tracker slider
            $('.tracker').slider({
                range: 'min',
                min: 0, max: 10,
                start: function(event,ui) {},
                slide: function(event, ui) {
                    Drupal.tonspion_7digital_player.song.currentTime = ui.value;
                },
                stop: function(event,ui) {}
            });
        

      }
      
      

    } // end else (!html5)

  
    
  }
}

Drupal.tonspion_7digital_player.insertTracks = function () {

  
  var settings = Drupal.settings.tonspion_7digital_player;
  var audioWrapper = $('#' + settings.audio_wrapper_id);

  // Grab album data
  $.get(
    "http://api.7digital.com/1.2/release/tracks",
    {
         releaseId: settings.album_id,
         oauth_consumer_key: settings.oauth_consumer_key
    },
    function(xml){
        $.each($(xml).find("errorMessage"), function(){
            //$("#defaults").append('<div id="console" class="grid-11 lastchild"><div class="messages warning">7digital player :: error :: '+$(this).text()+'</div></div>');
        });
      
        // add a link to each track below the album's image
        $.each($(xml).find("track"), function(key, track){
            var url = $(track).find('url').first().text();
            var trackId = $(track).attr('id');
      
            Drupal.tonspion_7digital_player.setTrack(trackId , $(track).find('title').first().text() , $(track).find('artist').first().find('name').text() , url);

            
        });
        
    }
  );

};

Drupal.tonspion_7digital_player.setTrack = function (trackId,title,artist,link) {

  var settings = Drupal.settings.tonspion_7digital_player;
  var audioWrapper = $('#' + settings.audio_wrapper_id);

  $.get(
    "http://api.7digital.com/1.2/track/preview",
    {
        trackId: trackId,
        redirect: false, // we don't want to be redirected to the file
        oauth_consumer_key: settings.oauth_consumer_key
    },
    function(xml) {
        var url = $(xml).find("url").text();
        //<li audiourl="01.mp3" cover="cover1.jpg" artist="Artist 1">01.mp3</li>
        audioWrapper.find('ul.playlist').append('<li audiourl="' + url + '" artist="' + artist + '" buylink="' + link + '">' + title + '</li>')
        //audioPlayer.attr("src", url).trigger("play"); // set url of song on player
        
        // initialization - first element in playlist
        Drupal.tonspion_7digital_player.initAudio($('.playlist li:first-child'));
    }
  );     

};

Drupal.tonspion_7digital_player.initAudio = function (elem) {
            var url = elem.attr('audiourl');
            var title = elem.text();
            var artist = elem.attr('artist');

            $('.player .title').text(artist + ' - ' + title);
            
            Drupal.tonspion_7digital_player.song = new Audio(url);
            Drupal.tonspion_7digital_player.song.volume = 0.3;



            // timeupdate event listener
            Drupal.tonspion_7digital_player.song.addEventListener('timeupdate',function (){
                var curtime = parseInt(Drupal.tonspion_7digital_player.song.currentTime, 10);
                $('.tracker').slider('value', curtime);
            });

            $('.playlist li').removeClass('active');
            elem.addClass('active');
            $('.player .buy a').attr('href',elem.attr('buylink'));
        }

Drupal.tonspion_7digital_player.playAudio = function () {
            Drupal.tonspion_7digital_player.song.play();

            $('.tracker').slider("option", "max", Drupal.tonspion_7digital_player.song.duration);

            $('.play').addClass('hidden');
            $('.pause').addClass('visible');
        }

Drupal.tonspion_7digital_player.stopAudio = function () {
            Drupal.tonspion_7digital_player.song.pause();

            $('.play').removeClass('hidden');
            $('.pause').removeClass('visible');
        }


})(jQuery);