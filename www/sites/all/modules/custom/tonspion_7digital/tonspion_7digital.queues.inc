<?php

/**
 * @file
 * Tonspion 7Digital - queues workers
 *
 * @author  leandro.vazquez@devtopia.coop
 */


/**
 * Process a release item
 *
 * -For each new release, check if exists in Tonspion
 * -If album not exist check if Artist exist in Tonspion
 * -If artist doesn't exist, skip release (do nothing). (CHANGED ON 31/10/2014 by TECNILOGICA. It was: If artist not exist add new Artist)
 * -If album not exist, add new Album
 * -If album exist and it has no player, add 7Digital player
 *
 * @param $xml_release xmlSimpleElement with release
 *
 * @return void
 **/
function tonspion_7digital_worker_release( $xml_release ){
  if (empty( $xml_release )) die('ERROR: no data in queue \n\r');

  $artists_nids = array();

  //Check if release exists in Tonspion
  $matching_albums = tonspion_7digital_get_albums_matching_7D( $xml_release );

  //If album is new (not exist in tonspion) check if Artist exists in Tonspion
  if (empty($matching_albums)){

	//DEBUG:
	//_my_drush_log( $xml_release->title . " is new album", 'ok' );

    $artists_nids = tonspion_7digital_get_artists_matching_7D( $xml_release->artist );

    // This block was CHANGED ON 31/10/2014 by TECNILOGICA. Before if was: If artist not exist, add new Artist
    // If artist doesn't exist, skip release
    if ( !$artists_nids ) {
	   _my_drush_log( "artist doesn't exist in db, release skipped: " . $xml_release->artist->name, 'ok' );

    } else {

      //Add new Album to first matching artist
      tonspion_7digital_add_album_from_7D( $xml_release, $artists_nids[0] );

      _my_drush_log("new album inserted: " . $xml_release->title  , 'ok' );   
    }

  } else {
    //If album exist and it has no player, add 7Digital player.

  	foreach ($matching_albums as $match) {
  		if (!is_numeric(@$match->field_ref_artist['und'][0]['target_id'])
  		     ||
  		    !is_numeric(@$match->field_genre['und'][0]['tid'])
  		  ) 
      {
  		  tonspion_7digital_update_album_from_7D( $match, $xml_release, null, null );
  		}
      else
      {
  		  //DEBUG
  		  _my_drush_log( $match->title . " album already in database", 'ok' );
  		}
  	}

  }


}

/**
 * Process an existing album
 *
 * -Add all album genres from 7Digital
 * -If album has no player, add 7Digital player
 *
 * @return void
 **/
function tonspion_7digital_worker_old_album( $xml_release ){

  // Check if release ID is stored in any album
  $query = new EntityFieldQuery;

  $result = $query
  ->entityCondition('entity_type', 'node')
  ->entityCondition('bundle', 'album')
  ->fieldCondition('field_album_sevendigital_id', 'value', $xml_release->{"@attributes"}->id, '=')
  ->execute();

  if (isset($result['node'])){

    $nid = array_keys($result['node']);
    $artist = node_load($nid[0]);

    if ( is_object($album) && is_object($xml_release) &&
         ( !is_numeric( $album->field_ref_artist['und'][0]['target_id'] ) || !is_numeric($album->field_genre['und'][0]['tid']) )
       ) {
		// avoid processed albums older than a year
		if ( strtotime( $album->field_album_sd_update_time['und'][0]['value'] ) < strtotime('-1 year', time()) ) {
			_my_drush_log( $xml_release->title . " older than a year, ignored and not artist/tags updated", 'notice');
			return;
		}

		tonspion_7digital_update_album_from_7D( $album, $xml_release ); //TODO: avoid processed data
		_my_drush_log( $xml_release->title . " artist & tags updated", 'ok');

	}else{

		_my_drush_log( $xml_release->title . " already has artist & tags", 'ok');

	}

  }else {

		_my_drush_log( $xml_release->title . " no album found in Drupal for this 7Digital release, inserting..", 'warning' );

		/************* CAUTION ******************/
		/*** CURRENT ALBUM IN ALBUMS UPDATE  ****
        /*** DOESN'T EXIST, ABOVE CODE       ****
        /*** INSERTS IT. COMMENT IF INSERTION ***
        /*** IS NOT DESIRED.                   ***
		/************* CAUTION ******************/
		$artist_nid = _create_artist_from_7D( $xml_release->artist );
		_create_album_from_7D( $xml_release, $artist_nid );
		_my_drush_log( $xml_release->title . " album inserted", 'ok');

  }
}

/**
 * Process an album existing in Drupal
 *
 * - If artist has no image, add 7Digital image
 * - If album has not artist, insert artist from /Digital
 *
 * @return void
 **/
function tonspion_7digital_worker_old_artist( $xml_release ){

  // Check if artist ID is stored as artist
  $query = new EntityFieldQuery;

  $result = $query
  ->entityCondition('entity_type', 'node')
  ->entityCondition('bundle', 'artist')
  ->fieldCondition('field_artist_sevendigital_id', 'value', $xml_release->{"@attributes"}->id, '=')
  ->execute();

  if ( isset($result['node']) ){

    $nids = array_keys($result['node']);
    foreach ( $nids as $nid ) {
		$artist = node_load( (int)$nid );

		if ( is_object($artist) && is_object($xml_release) ) {

			// insert Artist & Tags if aren't present
			if ( !is_numeric( $artist->field_artist_image['und'][0]['fid']) ) {

				// @TODO: avoid processed data

				_update_image_artist_from_7D( $artist, $xml_release );
				_my_drush_log( $xml_release->name . "(nid ". $artist->nid .") been updated his image", 'ok');

			}else{
				_my_drush_log( $xml_release->name . " already has image", 'ok');
			}

		}else {
			//TODO: watchdog
			_my_drush_log( $xml_release->name . " no artist found in Drupal for this 7Digital release", 'error' );
		}
	}

  }else{
    _my_drush_log($xml_release->name . " not found in Drupal database, inserting ...", "warning");
    tonspion_7digital_add_artist_from_7D( $xml_release );

  }

}
