<?php

/**
 *  Theme for mp3 album player
 */
function theme_tonspion_7digital_album_player($variables) {
	
	$album  = $variables['album'];
	$artist = $variables['artist'];

	// Album cover
	$album_cover = array(
	  'style_name' => 'media_200',
	  'path' => $album->field_album_image['und'][0]['uri'],
	  'width' => '200',
	  'height' => '200',
	  'alt' => $album->field_album_image['und'][0]['field_file_image_alt_text'],
	  'title' => $album->field_album_image['und'][0]['field_file_image_title_text'],
	  );

	// Genres
	$genres = '';
	if ( !empty($album->field_genre) ) {
		foreach ( $album->field_genre['und'] as $tid ) {
			$term = taxonomy_term_load($tid['tid']);
			$genres .= '<span>' . $term->name . '</span>';
		}
	}

	// Label link
	$label_link = '';
	if ( !empty($album->field_album_label['und'][0]['tid']) ) {
		$term = taxonomy_term_load($album->field_album_label['und'][0]['tid']);
		if(!empty($term)){
			$label_link = '<span class="label">' . t('Label') . ':</span> '. l($term->name, 'taxonomy/term/'.$term->tid); 
		}
	}

	// Artist link
	$artist_link = '';
	if ( !empty($artist->field_artist_weblink) ) { 
		$firstlink = array_shift($artist->field_artist_weblink['und']);
		$artist_link = l($firstlink['url'], $firstlink['url'], array('attributes'=>array('target'=>'_blank')));
	}

	// Rating
	$stars = 5;
	$rating_value = $album->field_album_redaction_rating['und'][0]['value'];
	$rating = '<span class="label">' . t('Redaction rating') . '</span> ';
	for ( $i=0; $i<$stars ; $i++ ) {
		$class = $i<$rating_value ? 'on' : 'off';
		$rating .= '<span class="star ' . $class . '"></span> ';
	}

	$player = !empty($album->field_album_sevendigital_id) && $album->field_album_sevendigital_id['und'][0]['value'] != '' ;

	$output = '
		<div id="sevendigital-player-wrapper">
			<div class="7digital-player-mp3-data ds-2col clearfix">
				<div class="group-left">
					' . theme('image_style',$album_cover) . '
				</div>
				<div class="group-right">
					<div class="mp3-tags">
					' . $genres . '
					</div>
					<h4 class="mp3-title">
					' . l($artist->title, 'node/'.$artist->nid) . ' - ' . $album->title . '
					</h4>
					<div class="mp3-label">
					' . $label_link . '
					</div>
					<div class="mp3-stars">
					' . $rating . '
					</div>
					<!--div class="mp3-url">
					' . $artist_link . '
					</div-->
				</div>
			</div>';
	if ( $player ) { 
		$output .= '
			<div class="player">
	            <div class="controls">
	            	<div class="rew control"></div>
	                <div class="play control"></div>
	                <div class="pause control"></div>
	                <div class="fwd control"></div>
	                <div class="tracker-wrapper">
	                	<div class="title"></div>
	                	<div class="tracker"></div>
	                </div>
	                <div class="buy control"><a href="" target="_blank" title="'. t('Musik kaufen') .'">'. t('Musik kaufen') .'</a></div>
	                <div class="logo control"></div>
	            </div>
	            
	        </div>
	        <ul class="playlist hidden">
	        </ul>
	        ';
	    }
	$output .= '
		</div>
	';
	

	
	return $output;
}

?>