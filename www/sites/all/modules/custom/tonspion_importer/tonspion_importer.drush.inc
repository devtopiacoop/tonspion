<?php

function tonspion_importer_drush_command() {
  $items = array();
  $items['tonspion-import-setinit'] = array(
    'description' => dt('Run import patch.'),
    'callback' => '_tonspion_importer_setinit'
  );
  $items['tonspion-import-label'] = array(
    'description' => dt('Run import patch.'),
    'callback' => '_tonspion_importer_update_label_album'
  );
  $items['tonspion-import-href'] = array(
    'description' => dt('Run import patch.'),
    'callback' => '_tonspion_importer_update_url_textarea',
    'arguments'   => array(
      'arg1'    => dt('An optional content type'),
    ),
  );
  $items['tonspion-import-fromtype'] = array(
    'description' => dt('Run import patch.'),
    'callback' => '_tonspion_importer_import_from_drush',
    'arguments'   => array(
      'type'    => dt('An optional content type'),
    ),
  );
  $items['tonspion-import-ref-update'] = array(
    'description' => dt('Entity references update.'),
    'callback' => '_tonspion_importer_ref_update_drush',
    'arguments'   => array(
      'arg1'    => dt('An optional content type'),
    ),
  );
  $items['tonspion-import-date-update'] = array(
    'description' => dt('Entity references date update.'),
    'callback' => '_tonspion_importer_date_update_drush',
    'arguments'   => array(
      'arg1'    => dt('An optional content type'),
    ),
  );
  $items['tonspion-import-create-redirects'] = array(
    'description' => dt('Create redirects.'),
    'callback' => '_tonspion_importer_create_redirects_drush'
  );
  $items['tonspion-import-update-redirects'] = array(
    'description' => dt('Update redirects for download/mp3.'),
    'callback' => '_tonspion_importer_update_redirects_drush'
  );
  $items['tonspion-import-create-mediatext'] = array(
    'description' => dt('Import media texts.'),
    'callback' => '_tonspion_importer_create_mediatext_drush'
  );
  $items['tonspion-import-update-maped-nids'] = array(
    'description' => dt('Update mapped nids table which were not registered.'),
    'callback' => '_tonspion_importer_update_mapped_drush',
    'arguments'   => array(
      'arg1'    => dt('An optional content type'),
    ),
  );
  return $items;
}

?>