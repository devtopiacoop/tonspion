<?php

/**
 * @file
 * Contains Eventim XMLReader Parser.
 *
 * Functions in this file are independent of the Feeds specific implementation.
 */

define ("EVENTIM_EVENTSERIE", 'eventserie');

/**
 * Class definition for EventimXMLReader that extend XMLReader methods
 */
class EventimXMLReader extends XMLReader
{

private $currentEventSeriePos;

  /**
   * Constructor.
   */
  public function __construct($config = array()) {
    $this->currentEventSeriePos = 0;
  }

  public function readToNextElement()
  {
      while (
          $result = $this->read()
          and $this->nodeType !== self::ELEMENT
      ) ;
      return $result;
  }

  public function readToNextEventSerie()
  {
      while (
          $result = $this->readToNextElement()
          and $this->localName !== EVENTIM_EVENTSERIE
      ) ;
      $this->currentEventSeriePos++;
      return $result;
  }

  public function moveToEventSerie($index = 0)
  {

      while (
          $this->lastEventSeriePos() < $index
          and $result = $this->readToNextEventSerie()
      ) ;
      
      return $result;
  }

  public function readToNextChildElement($depth)
  {
      // if the current element is the parent and
      // empty there are no children to go into
      if ($this->depth == $depth && $this->isEmptyElement) {
          return false;
      }

      while ($result = $this->read()) {
          if ($this->depth <= $depth) return false;
          if ($this->nodeType === self::ELEMENT) break;
      }

      return $result;
  }

  public function getNodeValue($default = NULL)
  {
      $node = $this->expand();
      return $node ? $node->nodeValue : $default;
  }

  public function lastEventSeriePos()
  {
    return $this->currentEventSeriePos;
  }

  public function rewind(){
    $this->currentEventSeriePos = 0;
  }


}


